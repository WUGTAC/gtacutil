from builtins import str
from builtins import object
import itertools as it
import re
import argparse
import logging

import attr
from path import Path

import gtac.util

logger = logging.getLogger(__name__)


class SeqDataError(Exception):
    pass


class BadSampleKey(Exception):
    pass


@attr.s
class IndexPair(object):
    index = attr.ib()
    sample = attr.ib()


def read_gtac_samplekey(fh):
    data = []
    for line in fh.readlines():
        m = re.search(r'(?P<index>[ACGT-]{4,})[ \t]+(?P<sample>.*)$',
                      line.strip())
        data.append(IndexPair(**m.groupdict()))
    sampleKey = SampleKey(data=data)
    return sampleKey


@attr.s
class SampleKey(object):
    # list of IndexPair objects
    data = attr.ib()

    def get_samples(self, index):
        for each in self.data:
            if each.index == index:
                return each.sample
        return None

    def is_valid(self):
        if len(set(e.index for e in self.data)) < len(self.data):
            raise BadSampleKey('Duplicate indexes in samplekey: %s', str(self))
        if len(set(e.sample for e in self.data)) < len(self.data):
            raise BadSampleKey('Duplicate sample names in samplekey: %s',
                               str(self))

    def __str__(self):
        return '\n'.join('\t'.join((e.index, e.sample)) for e in self.data)


class SequenceDataFactory(object):
    def __init__(self):
        self.methods = []

    def register(self, constructor, *args, **kwargs):
        self.methods.append((constructor, args, kwargs))

    def build(self, seq_data):
        data_errors = []
        for method in self.methods:
            # check that token is recognized by the method
            if method[0].valid_token(seq_data):
                # check that method constructor throws no SeqDataError
                try:
                    d = method[0](seq_data, *method[1], **method[2])
                except SeqDataError as e:
                    data_errors.append(e)
                else:
                    # check that get_sample_to_fastq throws no SeqDataError
                    try:
                        d.get_sample_to_fastq()
                    except SeqDataError as e:
                        data_errors.append(e)
                    # If no errors, return the object
                    else:
                        return d

        # Token recognized by one or more for our methods but there was some problem
        if data_errors:
            raise argparse.ArgumentTypeError(
                '\n'.join(str(e) for e in data_errors))
        # Token recognized by none of the registered SequenceData classes
        else:
            raise argparse.ArgumentTypeError(
                'The passed sequence data name {0} '
                'is not recognized by the registered methods: {1}'.format(
                    seq_data, ' '.join(str(m) for m in self.methods)))


class SequenceData(object):
    '''
    Interface for objects to parse sequence run names and fetch fastq files
    into analysis directories
    '''
    MAX_READS = 2  # Max number of fastqs a sample can have associated with it

    @classmethod
    def valid_token(cls, token):
        '''
        :param str token: name of sequence run
        :return: tuple (str, bool)
        '''
        raise NotImplementedError()

    def get_sample_to_fastq(self):
        '''
        returns dictionary of sample name to list of file paths
        '''
        raise NotImplementedError()

    def __str__(self):
        return str(self.data)


class DirectoryData(SequenceData):
    '''
    This class is for retrieving fastqs from a directory path
    containing fastqs named <sample>_[12] or <sample>_R[12]
    '''
    fastq_regexp = re.compile(r'(.*)_R?([12])\.f(?:ast)?q(?:\.\w+)?')

    def __init__(self, data_dir):
        self.data = Path(data_dir).expand().abspath()

    @classmethod
    def valid_token(cls, token):
        return Path(token).expand().isdir()

    def get_sample_to_fastq(self):
        d = {}
        search = [(fp, self.fastq_regexp.search(fp.basename()))
                  for fp in self.data.glob('*')]
        match_list = [m for _, m in search if m]
        no_match = [fp for fp, m in search if not m]
        if not match_list:
            raise SeqDataError(
                "No fastqs in dir %s found matching pattern" % self.data)
        if no_match:
            logger.warning('These files in dir %s are ignored: %s', self.data,
                           no_match)

        sort_matches = sorted(match_list, key=lambda x: x.group(1, 2))
        for k, grp in it.groupby(sort_matches, key=lambda x: x.group(1)):
            d[k] = [self.data.joinpath(m.string) for m in grp]
        return d

    def __str__(self):
        return self.data.basename()


class MiseqData(SequenceData):
    '''
    This class is for retrieving miseq data where the base
    calls have been moved into their own directory under demux_dir
    ie the case currently on the htcf
    We trigger this code by passing a gtac miseq run name
    ie R302
    '''
    empty_sample_name = 'Undetermined'

    def __init__(self, flowcell_id, demux_base='.'):
        self.data = flowcell_id.upper()
        self.demux_dir = Path(demux_base).joinpath(self.data)
        if not self.demux_dir.exists():
            raise SeqDataError(
                'target demux dir does not exist : %s ' % self.demux_dir)

    @classmethod
    def valid_token(cls, token):
        return bool(re.match(r'R\d+$', token, re.I))

    def get_sample_name(self, fp):
        try:
            return re.search(r'(.*?)_S\d+_L001_R([12])_001\.fastq\.gz',
                             fp.basename()).group(1)
        except AttributeError:
            return None

    def get_sample_to_fastq(self):
        '''
        Assumes that each sample name is unique in the demux dir
        '''
        sorted_files = sorted(
            self.demux_dir.glob('*'),
            key=lambda x: (self.get_sample_name(x) or '', x))

        d = {
            g: list(fp)
            for g, fp in it.groupby(sorted_files,
                                    lambda x: self.get_sample_name(x))
            if g != self.empty_sample_name and g is not None
        }

        if not d:
            raise SeqDataError(
                'no fastq files in dir %s found matching pattern' %
                self.demux_dir)
        return d

    def get_demux_fastq(self):
        fastqs = [
            fp for fp in Path(self.demux_dir).glob('*.fastq.gz')
            if self.get_sample_name(fp) != self.empty_sample_name
        ]
        if not fastqs:
            raise SeqDataError(
                'no fastq files found in directory %s' % self.demux_dir)
        return fastqs


class RawMiseqDemux(MiseqData):
    '''
    Accepts a path to a directory contained demuxed fastqs
    named in the Miseq convention
    '''

    def __init__(self, demux_dir):
        self.data = Path(demux_dir).abspath()
        self.demux_dir = self.data

    @classmethod
    def valid_token(cls, token):
        p = Path(token)
        return p.exists() and p.isdir()

    def get_demux_fastq(self):
        fastqs = [
            fp for fp in self.data.glob('*.fastq.gz')
            if self.get_sample_name(fp) != self.empty_sample_name
        ]
        if not fastqs:
            raise SeqDataError(
                'no fastq files found in directory %s' % self.data)
        return fastqs


class HiseqData(SequenceData):
    '''
    Assumes that in an increasing string sort the read1 filename will
    come before the read2 filename of the same sample
    '''
    index_regexp = re.compile(r'[ACGT-]{5,}')

    def __init__(self,
                 run_id,
                 sampleKey=None,
                 demux_base='.',
                 sample_key_base='.'):
        self.data = run_id
        self.demux_dir = Path(demux_base).joinpath(self.data)
        if not self.demux_dir.exists():
            raise SeqDataError(
                'target demux dir does not exist : %s ' % self.demux_dir)

        self.sample_key_base = Path(sample_key_base).abspath()

        self.sampleKey = self.make_sampleKey(
        ) if sampleKey is None else sampleKey

        try:
            self.sampleKey.is_valid()
        except BadSampleKey as e:
            raise SeqDataError(str(e))

    @classmethod
    def valid_token(cls, token):
        return bool(re.match(r'\d{3,5}_\d', token))

    def get_sample_key_path(self):
        keys = self.sample_key_base.glob('%s*' % self.data)
        # No keys found, bad
        if len(keys) == 0:
            raise SeqDataError(
                'Found no keys in directory %s ;' % self.sample_key_base.glob)
        # Interactively choose one of several discovered keys
        elif len(keys) > 1:
            print('Multiple samplekeys found, please choose one: ')
            try:
                return gtac.util.list_prompt(keys)
            except OSError as e:
                raise SeqDataError(str(e))
        # One key found, return the one true sample key
        else:
            return keys[0]

    def get_sample_name(self, fp):
        m = self.index_regexp.search(fp.basename())
        if m is None:
            return None
        else:
            index = m.group(0)

        sample = self.sampleKey.get_samples(index)
        if sample is None:
            logger.warning('index %s not found in sampleKey', index)

        return sample

    def get_sample_to_fastq(self):
        sorted_files = sorted(
            self.get_demux_fastq(), key=lambda x: (self.get_sample_name(x), x))

        d = {}
        for g, fp in it.groupby(sorted_files,
                                lambda x: self.get_sample_name(x)):
            d[g] = list(fp)
            if len(d[g]) > self.MAX_READS:
                logger.warning('Sample %s has too many fastqs %s', g, d[g])
        return d

    def get_demux_fastq(self):
        fastqs = [
            fp
            for fp in gtac.util.regexp_glob(self.demux_dir,
                                            r'.*?f(?:ast)?q(?:\.gz)?$')
            if self.get_sample_name(fp) is not None
        ]
        if not fastqs:
            raise SeqDataError(
                'no fastq files found in directory %s' % self.demux_dir)
        return fastqs

    def make_sampleKey(self):
        '''
        Assumes that file is a space or tab separated file and that
        the sample name follows the index column.  Takes everything
        from end of index to end of line as the sample and replaces
        spaces with _
        '''
        with open(self.get_sample_key_path(), 'r') as f:
            return read_gtac_samplekey(f)


class HiseqDataIndex(HiseqData):
    '''
    This class is for pipelines that want to name the samples after the index
    Does not require a sample_key_base path
    '''

    def __init__(self, run_id, demux_base='.'):
        self.data = run_id
        self.demux_dir = Path(demux_base).joinpath(self.data)
        if not self.demux_dir.exists():
            raise SeqDataError(
                'target demux dir does not exist : %s ' % self.demux_dir)

    def get_sample_name(self, fp):
        m = self.index_regexp.search(fp.basename())
        try:
            return m.group(0)
        except AttributeError:
            return None
