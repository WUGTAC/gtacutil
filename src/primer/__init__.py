from builtins import object
import attr

__version__ = "0.1.0"

REV_BASE = {
    'A': 'T',
    'T': 'A',
    'C': 'G',
    'G': 'C',
    'N': 'N',
    'W': 'W',
    'S': 'S',
    'M': 'K',
    'K': 'M',
    'R': 'Y',
    'Y': 'R',
    'B': 'V',
    'V': 'B',
    'D': 'H',
    'H': 'D',
    'I': 'N',  #not actually iupac
}

IUPAC_MAP = {
    'A': set(['A']),
    'C': set(['C']),
    'G': set(['G']),
    'T': set(['T']),
    'W': set(['A', 'T']),
    'S': set(['C', 'G']),
    'M': set(['A', 'C']),
    'K': set(['G', 'T']),
    'R': set(['A', 'G']),
    'Y': set(['C', 'T']),
    'B': set(['C', 'G', 'T']),
    'D': set(['A', 'G', 'T']),
    'H': set(['A', 'C', 'T']),
    'V': set(['A', 'C', 'G']),
    'N': set(['A', 'C', 'G', 'T', 'N']),
    'I': set(['A', 'C', 'G', 'T', 'N']),
}


@attr.s
class Amplicon(object):
    name = attr.ib()
    primers = attr.ib()
    revcomp_primers = attr.ib()

    def amplify(self, seqs):
        '''
        seqs is an iterable of Biopython SeqRecords
        returns an AmplifyResults object
        '''
        raise NotImplementedError()

    def primer_pairs(self, reverse=True):
        '''
        Returns an iterable of tuples
        Each tuple represents a combo of forward and revcomp primers
        such that one would see in a single strand of DNA in 5' to 3' order
        '''
        raise NotImplementedError()


@attr.s
class Primer(object):
    seq = attr.ib()

    def bind(self, target_seq):
        '''
        target_seq is string
        returns an iterator of tuples representing start and end of binding locations
        '''
        raise NotImplementedError()

    def __len__(self):
        return len(self.seq)
