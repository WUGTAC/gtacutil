import shutil
import os
import logging
import attr

logger = logging.getLogger(__name__)


def soft_link(target, link_name):
    try:
        os.symlink(target, link_name)
    except OSError as e:
        logger.warning(e)


def copy(target, link_name):
    shutil.copyfile(target, link_name)


def cat(target, link_name):
    # soft link if link doesn't exsist, else concatenate
    if link_name.exists():
        if link_name.islink():
            fp = link_name.realpath()
            link_name.remove()
            fp.copyfile(link_name)
        os.system('cat %s >> %s' % (target, link_name))
    else:
        soft_link(target, link_name)


_METHOD_DICT = {'copy': copy, 'link': soft_link, 'cat': cat}

method_names = list(_METHOD_DICT.keys())


@attr.s
class FetchItem(object):
    target = attr.ib()
    link = attr.ib()
    metadata = attr.ib(default=attr.Factory(dict))

    def __iter__(self):
        return iter((self.target, self.link))


def fetch_files(fetch_item_list, fetch_method='link'):
    '''
    :param list fetch_item_list: List of link.FetchItem objects
    :param str fetch_method: Method to use for fetching reads.
    :return: List of created file-paths
    :raises KeyError: if fetch_method is not recognized
    '''
    fetch_fn = _METHOD_DICT.get(fetch_method)
    if fetch_fn is None:
        raise KeyError('unknown fetch method %s' % fetch_method)

    for item in fetch_item_list:
        fetch_fn(item.target, item.link)
    return [each.link for each in fetch_item_list]
