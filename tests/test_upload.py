from path import Path

import gtac.upload
from gtac.upload import resolve, method


def test_dir_method(tmpdir):
    tmpdir = Path(str(tmpdir))
    gtac.upload.method_dict['dir'](Path.getcwd().glob('ssu_test_r*'), tmpdir)
    assert len(tmpdir.glob('ssu_test_r*')) == 2
    gtac.upload.method_dict['dir'](
        Path.getcwd().glob('ssu_test_r*'),
        tmpdir,
        method=method.tarball_cp,
        basename='test.tar')
    assert tmpdir.joinpath('test.tar').exists()
