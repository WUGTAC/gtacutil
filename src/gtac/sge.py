import functools
import sh
import os
import re
from datetime import timedelta


def _get_jobid_from_qsub(qsub_stdout):
    return re.search(r'\d+', qsub_stdout).group(0)


def _submit_job(submit_cmd, cmd, cmd_args=None, drm_flags=''):
    '''
    cmd_args is a list and drm_flags is a string
    '''
    if cmd_args is None:
        cmd_list = [cmd]
    else:
        cmd_list = [cmd] + cmd_args
    ## if hasattr(cmd_args, '__iter__'):
    ##     cmd_args_str = cmd + ' ' + ' '.join(cmd_args).strip()
    ## else:
    ##     cmd_args_str = cmd + ' ' + cmd_args.strip()
        
    return submit_cmd(*drm_flags.split(), _in=' '.join(cmd_list))


def submit_job(
        cmd,
        cmd_args=None,
        name=None,
        time=timedelta(minutes=30),
        workers=1,
        memInGB=1,
        hold=None,
        logdir=None,
        concurrent=None,
        copyEnv=True,
        workDir=os.getcwd(),
        extra=''):
    '''
    Assumes that jobs are run with all env variables exported and in cwd
    Assumes memory is in GB
    Concurrent is a function which gets passed (workers, memInGB)
    hold is a list of job_ids that must complete before this job is run
    extra is a string that will be appended to the drm flag string
    NEED TO THINK ABOUT hold=[None]
    '''
    long_time = timedelta(hours=1)

    qsub_flags = []
    
    if copyEnv:
        qsub_flags.append('-V')

    qsub_flags.append('-wd %s' % workDir)

    if long_time <= time:
        qsub_flags.append('-P long')
    
    if name:
        qsub_flags.append('-N %s' % name)

    if concurrent is not None:
        assert workers > 1, \
          'Tried to call concurrent with %s workers' % workers        
        qsub_flags.append(concurrent(workers, memInGB))

    if logdir:
        qsub_flags.append('-e %s' % logdir)
        qsub_flags.append('-o %s' % logdir)

    if hold:
        work_hold = (h for h in hold if h is not None)
        qsub_flags.append('-hold_jid %s' % ','.join(hold))
        
    qsub_flags.append('-l h_vmem={:.2f}G'.format(memInGB + 0.01))
    qsub_flags.append(extra)

    qsub_flags_str = ' '.join(qsub_flags)
    cmd_args_list = [] if cmd_args is None else cmd_args
        
    return _submit_job(sh.qsub, cmd, cmd_args=cmd_args_list, drm_flags=qsub_flags_str)


def _novoalign_concurrent(worker, memInGB):
    assert worker >= 8, 'too many workers: %s' % worker
    return '-pe shared {0}'.format(worker)

def _standard_concurrent(worker, memInGB):
    assert worker >= 8, 'too many workers: %s' % worker    
    return '-pe shared {0}'.format(worker)

def _mpi_concurrent(worker, memInGB):
    assert worker >= 12, 'too many workers: %s' % worker
    return '-pe mpi {0}'.format(worker)


submit_parallel_job = functools.partial(submit_job, concurrent=_standard_concurrent)

submit_mpi_job = functools.partial(submit_job, concurrent=_mpi_concurrent)
                                   
submit_novoalign_job = functools.partial(submit_job, concurrent=_novoalign_concurrent)
