===============================
gtacUtil
===============================

| |docs| |travis| |appveyor| |coveralls| |landscape| |scrutinizer|
| |version| |downloads| |wheel| |supported-versions| |supported-implementations|

.. |docs| image:: https://readthedocs.org/projects/gtacUtil/badge/?style=flat
    :target: https://readthedocs.org/projects/gtacUtil
    :alt: Documentation Status

.. |travis| image:: http://img.shields.io/travis/aschriefer/gtacUtil/master.png?style=flat
    :alt: Travis-CI Build Status
    :target: https://travis-ci.org/aschriefer/gtacUtil

.. |appveyor| image:: https://ci.appveyor.com/api/projects/status/github/aschriefer/gtacUtil?branch=master
    :alt: AppVeyor Build Status
    :target: https://ci.appveyor.com/project/aschriefer/gtacUtil

.. |coveralls| image:: http://img.shields.io/coveralls/aschriefer/gtacUtil/master.png?style=flat
    :alt: Coverage Status
    :target: https://coveralls.io/r/aschriefer/gtacUtil

.. |landscape| image:: https://landscape.io/github/aschriefer/gtacUtil/master/landscape.svg?style=flat
    :target: https://landscape.io/github/aschriefer/gtacUtil/master
    :alt: Code Quality Status

.. |version| image:: http://img.shields.io/pypi/v/gtacUtil.png?style=flat
    :alt: PyPI Package latest release
    :target: https://pypi.python.org/pypi/gtacUtil

.. |downloads| image:: http://img.shields.io/pypi/dm/gtacUtil.png?style=flat
    :alt: PyPI Package monthly downloads
    :target: https://pypi.python.org/pypi/gtacUtil

.. |wheel| image:: https://pypip.in/wheel/gtacUtil/badge.png?style=flat
    :alt: PyPI Wheel
    :target: https://pypi.python.org/pypi/gtacUtil

.. |supported-versions| image:: https://pypip.in/py_versions/gtacUtil/badge.png?style=flat
    :alt: Supported versions
    :target: https://pypi.python.org/pypi/gtacUtil

.. |supported-implementations| image:: https://pypip.in/implementation/gtacUtil/badge.png?style=flat
    :alt: Supported imlementations
    :target: https://pypi.python.org/pypi/gtacUtil

.. |scrutinizer| image:: https://img.shields.io/scrutinizer/g/aschriefer/gtacUtil/master.png?style=flat
    :alt: Scrutinizer Status
    :target: https://scrutinizer-ci.com/g/aschriefer/gtacUtil/

Python library for quickly building gtac pipelines

* Free software: BSD license

Installation
============

::

    pip install gtacUtil

Documentation
=============

https://gtacUtil.readthedocs.org/

Development
===========

To run the all tests run::

    tox
