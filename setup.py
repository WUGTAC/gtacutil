#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from __future__ import absolute_import, print_function

import io
import os
import re
import sys
import shutil
from glob import glob
from os.path import basename
from os.path import dirname
from os.path import join
from os.path import relpath
from os.path import splitext

from setuptools import find_packages
from setuptools import setup
from setuptools.extension import Extension


def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')).read()


requirements = [
    'sh', 'path.py', 'pytest', 'click', 'attrs', 'PyYAML', 'jinja2', 'future'
]

primer_requirements = [
    'python-Levenshtein',
    'biopython',
    'regex',
]

keymaster_requirements = [
    'fuzzywuzzy',
    'pandas>=0.16.1',
    'xlrd',
]

ext_modules = []

setup(
    name='gtacUtil',
    version='1.0.0',
    license='BSD',
    description='Python library for quickly building gtac pipelines',
    long_description='%s\n%s' %
    (read('README.rst'),
     re.sub(':[a-z]+:`~?(.*?)`', r'``\1``', read('CHANGELOG.rst'))),
    author='Andrew Schriefer',
    author_email='aschriefer@path.wustl.edu',
    url='https://github.com/aschriefer/gtacUtil',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    package_data={'primer': ['data/*']},
    py_modules=[splitext(basename(fp))[0] for fp in glob('src/*.py')],
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        # complete classifier list: http://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: Unix',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Topic :: Utilities',
    ],
    keywords=[
        # eg: 'keyword1', 'keyword2', 'keyword3',
    ],
    extras_require={
        'keymaster': keymaster_requirements,
        'primer': primer_requirements,
    },
    entry_points={
        'console_scripts': [
            'analysis_notes = gtac.analysis_notes:main',
            'primerTrimmer = primer.primerTrimmer:main [primer]',
            'keymaster = keymaster.__main__:cli [keymaster]',
        ]
    },
    ext_modules=ext_modules,
    install_requires=requirements, )

if sys.executable:
    bin_dir = dirname(sys.executable)
    for fp in glob('scripts/*'):
        shutil.copy(fp, bin_dir)
