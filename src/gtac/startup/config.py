import logging
import sys
import re
import yaml
import json
from path import Path

logger = logging.getLogger(__name__)

class UnknownConfigType(Exception):
    pass


def _load_factory(fp):
    if re.search(r'\.ya?ml$', fp):
        return yaml.load
    elif re.search(r'\.json$', fp):
        return json.load
    else:
        raise UnknownConfigType(fp)


def env_config_dir():
    d = Path(sys.executable).dirname()
    return d.parent.joinpath('etc').abspath()


def load_config(*paths):
    config = {}
    logger.info('config search path is: %s', paths)
    for fp in paths:
        if fp is None:
            continue
        elif not Path(fp).isfile():
            continue
        load = _load_factory(fp)
        with open(fp) as fh:
            logger.info('reading config from file: %s', fp)
            d = load(fh)
            if d is None:
                logger.info('config file is empty: %s', fp)
            config.update(d)
    if not config:
        logger.warning('no config information found')
    return config


def system_config(*paths):
    '''
    Takes a base config file name and reads and merges matching config files
    on a defined search path. This path is:
    1) dirname(python)/../etc/
    2) ~/.config/
    3) ./
    :Returns: dict
    '''
    search_path = [
        env_config_dir(), Path('~/.config/').expanduser(), Path.getcwd()
    ]
    files = [d.joinpath(*paths) for d in search_path]
    return load_config(*files)
