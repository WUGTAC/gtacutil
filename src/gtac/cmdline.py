import os
from argparse import ArgumentTypeError

from path import Path
'''
Collection of custom types to be used for variable cleaning in argparse
'''


def abs_exst_dir(fp):
    '''
    This follows readlink -f lead and declares the canonical way to write the path
    to a directory in unix is with no trailing /.  This allows Path(fp).name
    to work as expected instead of returning an empty string
    '''
    fp = Path(fp.rstrip(os.path.sep)).abspath()
    if not fp.isdir():
        raise ArgumentTypeError('directory %s does not exist!' % fp)

    return fp


def abs_exst_path(fp):
    fp = Path(fp).abspath()
    if not fp.exists():
        raise ArgumentTypeError('file %s does not exist!' % fp)

    return fp


def abs_nonexst_path(fp):
    fp = Path(fp).abspath()
    if fp.exists():
        raise ArgumentTypeError('file %s already exists!' % fp)

    return fp


def abs_path(fp):
    fp = Path(fp).abspath()
    return fp


def positive_int(int_):
    i = int(int_)
    if i <= 0:
        raise ArgumentTypeError(
            'argument is %s, it must be a positive integer' % i)

    return i


def non_negative_int(int_):
    i = int(int_)
    if i < 0:
        raise ArgumentTypeError(
            'argument is %s, it must be a positive integer' % i)

    return i
