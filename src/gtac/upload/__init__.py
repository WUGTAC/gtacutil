import functools as ft
import logging
from path import Path

from gtac.upload import resolve, method

'''
Upload counterpart to the startup module. For this one I went simpler.
I have upload directory paths hardcoded in the upload.resolve functions and I directly
create the methods in the module. There is no automagic parsing of the tokens to guess the method.
All this means no extra config file section is needed.
'''

try:  # Python 2.7+
    from logging import NullHandler
except ImportError:

    class NullHandler(logging.Handler):
        def emit(self, record):
            pass


def upload(file_list, output_token, resolve=None, method=None, **kwargs):
    output = resolve(output_token)
    relative_files = [Path(e).relpath() for e in file_list]
    return method(relative_files, output, **kwargs)


method_dict = {}
method_dict['htcf'] = ft.partial(upload, resolve=resolve.htcf, method=method.rsync)
method_dict['htcf_tar'] = ft.partial(upload, resolve=resolve.htcf, method=method.tarball_cp)
method_dict['dir'] = ft.partial(upload, resolve=lambda x: x, method=method.rsync)
