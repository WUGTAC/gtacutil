from __future__ import division
from past.utils import old_div
import os
import sh
import functools
import threading
import math
from datetime import timedelta

_LOCK = threading.Lock()
_JOB_NAME_TO_ID = {}

#processes per node for mpi novoalign (ie novoalign -c )
NOVOALIGN_MPI_PPN = 4

def format_mem(mem):
    '''
    pbs insists that memory resources are in format [integer][suffix]
    so we take ceiling of memory request
    '''
    return int(math.ceil(mem))
    
def format_timedelta(td):
    hours = td.days * 24 + old_div(td.seconds, 3600)
    minutes = td.seconds % 3600 / 60
    return '{0:02}:{1:02}:00'.format(hours, minutes)


def get_jobid_from_qsub(qsub_stdout):
    return qsub_stdout.strip()


def _submit_job(submit_cmd, cmd, drm_flags=''):
    '''
    cmd can be a list or a string
    '''
    return submit_cmd(*drm_flags.split(), _in=cmd)

        
    
def submit_job(
        cmd,
        name='',
        time=timedelta(minutes=30),
        workers=1,
        memInGB=1,
        hold=None,
        logdir=None,
        concurrent=None,
        copyEnv=True,
        workDir=None,
        extra=''
        ):

    '''
    cmd is the name of the executable/script plus the arguments to the command.
    cmd may be a string or a list.  Use a list if you do not want one token to
    be split on whitespace by shell ie reading a file called 'input file'
    
    cmd is echo'ed into the submission binary (ie qsub) so it is safe
    to submit a binary.
    
    name is a string that the job will be named.
    Submitting more than one job with the same name will throw an error.
    Only jobs submitted with a name can be used to hold future jobs
    
    hold is a list of strings that give the name of jobs that the current job should wait for.
    hold names are mapped to job ids via get_job_id_from_qsub() and _JOB_NAME_TO_ID
    Any entries in hold that evaluate to False will be ignored.
    Any entries in hold for which no job with that name has been submitted yet will be ignored.
    If all entries are ignored (or if hold is None) then hold_job flag will not appear in the qsub command
    The job will be set to run after the hold entries regardless of exit status.

    concurrent is a function that has signature: concurrent(int(workers), int(memInGB))
    concurrent's main responsibility is to set the submission queue info
      (ie mpi mode and number of nodes and/or processes required).
    If concurrent is not None then it is responsible for setting the memory requirement flag
    If no concurrent function is passed then memory requirment flag is set using format_mem(memInGB)

    copyEnv is a boolean.  If true the current process' environment (ie shell variables)
      is passed to the submitted job

    time is a datetime.timedelta object.  It represents the maximium amount of time the job should
      run for.

    workDir is the directory to use as working directory for the submitted job.
    This defaults to os.getcwd(). 

    logDir is directory to place error AND output streams of job

    extra is a string that is appended to the other submission flags
    '''
    qsub_flags = []
    
    if copyEnv:
        qsub_flags.append('-V')

    qsub_flags.append('-d {0}'.format(
        os.getcwd() if workDir is None else workDir
    ))
    
    if name:
        qsub_flags.append('-N %s' % name)


    if logdir:
        qsub_flags.append('-e %s' % logdir)
        qsub_flags.append('-o %s' % logdir)


    if hold:
        job_ids = (_JOB_NAME_TO_ID.get(n) for n in hold if n)
        job_id_string = ':'.join([jid for jid in job_ids if jid is not None])
        
        if job_id_string:
            qsub_flags.append('-W depend=afterany:%s' % job_id_string)
        else:
            pass

    resource_string = '-l '
    
    if concurrent is None:
        resource_string += 'vmem={:d}gb'.format(format_mem(memInGB))
    else:
        assert workers > 1, \
          'Tried to call concurrent with %s workers' % workers
        resource_string += concurrent(int(workers), format_mem(memInGB))
        
    resource_string += ',walltime=%s' % format_timedelta(time)

    qsub_flags.append(resource_string)
    
    qsub_flags.append(extra)

    qsub_flags_str = ' '.join(qsub_flags)

    with _LOCK:
        p = _submit_job(sh.qsub, cmd, drm_flags=qsub_flags_str)
        if name in _JOB_NAME_TO_ID:
            assert False, \
              'Name {0} already in _JOB_NAME_TO_ID with value {1}'.format(
                  name, _JOB_NAME_TO_ID.get(name))
            
        if name is not None:
            _JOB_NAME_TO_ID[name] = get_jobid_from_qsub(p.stdout)
            
    return p


def novoalign_concurrent(workers, memInGB):
    while workers % NOVOALIGN_MPI_PPN != 0:
        workers += 1
    return 'nodes={0:d}:ppn={1:d},vmem={2:d}gb'.format(
        old_div(workers, NOVOALIGN_MPI_PPN), NOVOALIGN_MPI_PPN, memInGB)


def standard_concurrent(workers, memInGB):
    assert workers <= 64, 'too many workers: %s' % workers
    return 'nodes=1:ppn={0:d},vmem={1:d}gb'.format(workers, memInGB)


submit_novoalign_job = functools.partial(submit_job, concurrent=novoalign_concurrent)

submit_parallel_job = functools.partial(submit_job, concurrent=standard_concurrent)
