"""
This module provides boilerplate code for parsing gtac sequence data ids
from the commandline and then linking the proper fastqs into an analysis directory

Usage
=====

Simplest way to use this module is:

.. code-block:: python

    import argparse
    from path import Path
    import gtac.startup as st

    factory, config = st.bootstrap()

    parser = argparse.ArgumentParser()
    parser.add_argument('--data', nargs='+', type=factory.build)
    parser.add_argument('-c', '--customer')

    args = parser.parse_args()

    analysis_dir = Path('_'.join([args.customer, st.multilane_id(args.data)]))
    analysis_dir.mkdir_p()

    def make_link_name(seq_data, sample, read_number):
        x = '{sample}.{seq_data}.R{read_number}.fq.gz'.format(**locals())
        return analysis_dir.joinpath(x)

    sample_table = analysis_dir.joinpath('samples.txt')
    st.write_sample_table(args.data, sample_table, make_link_name)
    st.fetch_fastq(sample_table, 'link')

This will create a sample table samples.txt with the original reads in the read columns and
the links to be made in the link columns. The links are named using make_link_name.
Ideally make_link_name should return an absolute path
to avoid the error that will occur if you cd between ``write_sample_table`` and ``fetch_fastq``.
Function ``fetch_fastq`` then copies the fastq files into the analysis directory using
the named method. See source of ``fetch_files`` for valid method names.

"""
from builtins import object
import os
from path import Path
from functools import partial

from gtac.startup.data import SequenceDataFactory, DirectoryData, HiseqData, MiseqData
from gtac.startup.link import fetch_fastq, write_sample_table, clean_sample_name
from gtac.startup.fetch_files import method_names as fetch_method_names
from gtac.startup.config import system_config

import logging
try:  # Python 2.7+
    from logging import NullHandler
except ImportError:

    class NullHandler(logging.Handler):
        def emit(self, record):
            pass


logging.getLogger(__name__).addHandler(NullHandler())


def bootstrap():
    '''
    This is a convenience function that lets you use this api quickly
    '''
    seqDataFactory = SequenceDataFactory()
    config = system_config('gtac-data.yaml')

    seqDataFactory.register(DirectoryData)

    for each in config.get('hiseq', []):
        seqDataFactory.register(HiseqData, **each)

    for each in config.get('miseq', []):
        seqDataFactory.register(MiseqData, **each)

    return seqDataFactory, config


class AnalysisDirectory(object):
    '''
    Assumes that all directories only contain one kind of file
    that will be needed for further processing

    For every key in dirs a method will be added to the instance
    The method is named after the key.  The method w/o any arguments
    gives you the directory.  The method with kwargs gives you the formated
    final file name. The template is fetch() defined in __init__().
    Each value is a tuple with name of the directory followed by a
    string that gives you the desired name through s.format(**kwargs)

    For every key in files a method will be added named after the key.
    This method takes no args and returns the name of the file.
    The file is assumed to reside in the top level of the analysis directory

    To-Do: make it more obvious why a call to fetch w/o using
    kwargs throws a TypeError.  Also this crap should probably be
    specified with yaml
    '''
    dirs = {'reads': '{sample}.R{read}.fq.gz'}
    files = {'summary': 'summary.txt'}

    def __init__(self, wd=None):
        def file_fetch(self, name):
            return self.path.joinpath(name)

        def fetch(self, dir_, pat, **kwargs):
            '''
            Use to return a specific directory
            or file name matching kwargs formatting
            '''
            d = self.path.joinpath(dir_)
            if not kwargs:
                return d

            formatted = Path(pat.format(**kwargs))
            if formatted.isabs():
                raise ValueError(
                    'formatted sub-path results in an absolute path: %s' %
                    formatted)

            return d.joinpath(formatted)

        self.path = Path.getcwd() if wd is None else Path(wd).abspath()

        #load dir methods
        for k, val in self.dirs.items():
            self.__dict__[k] = partial(fetch, self, k, val)

        #load file methods
        for k, val in self.files.items():
            self.__dict__[k] = partial(file_fetch, self, val)

    def create(self):
        self.path.mkdir_p()
        for d in self.dirs:
            self.__dict__[d]().mkdir_p()

    def __str__(self):
        return self.path
