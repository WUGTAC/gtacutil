from __future__ import unicode_literals
from builtins import str
from builtins import range
import os
import pytest
import random
import io
import pandas as pd
import primer
from primer import primerTrimmer as trim
from gtac.util import run_command
from path import Path

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import generic_dna

MIN_MATCH_LEN = 5


def convert_iupac(seq):
    mapped = (primer.IUPAC_MAP.get(e) for e in seq)
    return ''.join(random.sample(s, 1)[0] for s in mapped)


def make_read_pair_from_rec(rec, extra_trim=0, min_match_len=5, read_len=101):
    '''
    rec is a primer record which is a dictionary with 4 entries
    Returns a 2 tuples of form (full_read, read_minus_primers)
    '''
    seq_p2 = Seq(convert_iupac(rec['p2']), generic_dna)
    n1 = len(rec['p1'])
    n2 = len(seq_p2)
    S = int(rec['size'])

    bases = ['A', 'C', 'G', 'T']
    if rec.get('amplicon') is None:
        target = ''.join(random.choice(bases) for _ in range(S - n1 - n2))
        raw_amp_str = ''.join([
            convert_iupac(rec['p1']), target,
            str(seq_p2.reverse_complement())
        ])
        raw_amp = SeqRecord(
            Seq(raw_amp_str),
            id='amplicon',
            letter_annotations={'phred_quality': [30] * len(raw_amp_str)})

    else:
        raw_amp = SeqRecord(
            Seq(rec['amplicon']),
            id='amplicon',
            letter_annotations={'phred_quality': [30] * len(rec['amplicon'])})

    amp = raw_amp

    read_one = amp[:read_len]
    read_two = amp.reverse_complement()[:read_len]

    N = len(amp)
    # Need to just refactor this whole thing
    # This is wrong because we dont always want extra trim off 3prime
    span_one = (n1 + extra_trim, N if n2 - (N - read_len) < min_match_len else
                N - n2 - extra_trim)
    span_two = (n2 + extra_trim, N if n1 - (N - read_len) < min_match_len else
                N - n1 - extra_trim)

    return (
        rec['name'], \
        (read_one,read_one[span_one[0]:span_one[1]]), \
        (read_two,read_two[span_two[0]:span_two[1]])
         )


def simple_test():
    primer_recs = [{
        'p1': 'AAAAA',
        'p2': 'GGGGG',
        'size': '60',
        'amplicon': 'AAAAATTTTCCCCC',
        'name': 'test',
    }]

    return primer_recs


def shifted_primers():
    primer_recs = [{
        'p1': 'CGATGGGTAGTGAAGCAATGGTTTTG',
        'p2': 'ACGGCTACGAAAATCCAAC',
        'name': 'APP_CES2',
        'size': '108'
    }, {
        'p1': 'TGGGTAGTGAAGCAATGGTTTTG',
        'p2': 'CAGAACGGCTACGAAAATCCAAC',
        'name': 'APP_EX_18_1',
        'size': '110'
    }]

    return primer_recs


def test_dir():
    return Path(__file__).abspath().dirname()


def ssu_primers():
    fp = test_dir().joinpath('ssu_primers.txt')
    return trim.read_gtac_primer_file(fp).to_dict('records')


# yapf: disable
@pytest.mark.parametrize(
    'primer_recs,errors,extra_trim,min_match_len',
    [
        (simple_test(), 0, 0, 0),
        #pytest.mark.xfail((simple_test()*2, 0, 0, 0)),
        (shifted_primers(), 3, 0, 5),
        (shifted_primers(), 3, 1, 5),
        (ssu_primers(), 2, 1, 5),
        (ssu_primers(), 2, 0, 5),
        (ssu_primers(), 2, 1, 5),
    ])
# yapf: enable
def test_trim_primer(primer_recs, errors, extra_trim, min_match_len):
    def _check(reads_and_seqs):
        for name, (read_one, target_one), (read_two,
                                           target_two) in reads_and_seqs:
            pair = (read_one, read_two)
            ident = ident_fn(amplicon_lst, pair)

            result = trim.trim_primers(trimmer, ident)

            assert bool(result)
            assert [e.seq for e in result.product] == [
                target_one.seq, target_two.seq
            ]

    reads_and_seqs = \
      [make_read_pair_from_rec(
          rec,
          extra_trim=extra_trim,
          min_match_len=min_match_len,
          )
          for rec in primer_recs
      ]

    df = pd.DataFrame.from_records(
        primer_recs, columns=['name', 'p1', 'p2', 'size'])
    amplicon_lst = trim.construct_amplicons(df, errors=errors)
    trimmer = trim.PrimerTrimmer(
        extra_trim=extra_trim,
        min_match_len=min_match_len,
        iupac_map=primer.IUPAC_MAP)

    memoize = trim.build_memoize_identify(amplicon_lst)
    funcs = [
        trim.identify_origin_amplicon,
        memoize(trim.identify_origin_amplicon)
    ]

    for ident_fn in funcs:
        for rs in [reads_and_seqs, reads_and_seqs[::-1]]:
            _check(rs)


@pytest.mark.parametrize(
    'extra', [('--pickle %s' % os.devnull),
              ('-a {}'.format(test_dir().joinpath('adapters.fa'))), ('')])
def test_full_run_ssu(tmpdir, extra):
    dir_ = tmpdir.mkdir('sub')
    fps = [
        test_dir().joinpath(e)
        for e in ('ssu_test_r1.fq.gz', 'ssu_test_r2.fq.gz', 'ssu_primers.txt')
    ]
    out_fps = [dir_.join(e) for e in ('ssu_out_r1.fq.gz', 'ssu_out_r2.fq.gz')]
    fps += out_fps

    process = run_command('primerTrimmer -i {1} {2} -p {3} -o {4} {5} '
                          '{0}'.format(extra, *fps))

    assert process.returncode == 0
    assert all(Path(p).size > 1200000 for p in out_fps)


def test_devnull_output(tmpdir):
    dir_ = tmpdir.mkdir('sub')
    pkl = Path(dir_.join('test.pkl'))
    extra = '--pickle ' + pkl
    fps = [
        test_dir().joinpath(e)
        for e in ('ssu_test_r1.fq.gz', 'ssu_test_r2.fq.gz', 'ssu_primers.txt')
    ]
    fps += [os.devnull] * 4

    process = run_command(
        'primerTrimmer -i {1} {2} -p {3} -o {4} {5} -u {6} {7} '
        '{0}'.format(extra, *fps))

    assert process.returncode == 0
    assert pkl.size > 0

    process = run_command('primerTrimmer -i {1} {2} -p {3} -o {4} {5} '
                          '{0}'.format(extra, *fps))

    assert process.returncode == 0
    assert pkl.size > 0


def test_maximum_safe_errors():
    assert trim.maximum_safe_errors([]) == None
    assert trim.maximum_safe_errors([[], []]) == None
    assert trim.maximum_safe_errors([[], ['ACGT', 'TGCG']]) == None
    assert trim.maximum_safe_errors([['GATT'], ['ATTC']]) == 0
    assert trim.maximum_safe_errors([['ACGT', 'TGCG'], ['ACGT', 'TGCG']]) == 0
    assert trim.maximum_safe_errors([['ACGT', 'TGCG'], ['ACGTAA',
                                                        'TGCGTT']]) == 0
    assert trim.maximum_safe_errors([['GGCCC', 'TAAAC'], ['ACGTA',
                                                          'TGCGA']]) == 2


def test_adapter_prior():
    df = pd.DataFrame.from_dict({'size': [150, 101, 60, 102]})
    assert trim.find_adapter_contamination_prior(df, 101) == 0.25

    df = pd.DataFrame.from_dict({'size': [150, 101, 60, pd.np.nan]})
    assert trim.find_adapter_contamination_prior(df, 101) is None

    df = pd.DataFrame.from_dict({'size': []})
    assert trim.find_adapter_contamination_prior(df, 101) == 0


def test_primer_file_no_size():
    # yapf: disable
    strings = [
'''amp,AAA,TTT
amp2,CCC,GGG''',
 '''amp,AAA,TTT,150
amp2,CCC,GGG'''
    ]
    #yapf: enable
    for s in strings:
        tab_s = s.replace(',', '\t')
        assert trim.find_adapter_contamination_prior(
            trim.read_gtac_primer_file(io.StringIO(tab_s)), 101) is None
