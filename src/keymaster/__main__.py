from __future__ import absolute_import
from builtins import str
import os
import re
import pandas as pd
import tempfile
import logging
import click
from path import Path

import keymaster.check_id_map as check_id_map

import gtac.util

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

'''
A note about qiime sample keys:
 A valid Qiime sample has a header where the first entry is #SampleID and the last is Description
see this site for more details:
http://qiime.org/documentation/file_formats.html#metadata-mapping-files

Usage:
convert a customer metadata table into a qiime sample key with SampleID named after Mouse
 keymaster.py load metadata.csv convert -i Mouse save output.txt

convert two customer metadata tables and concat on SampleID dropping duplicate IDs
 keymaster.py load metadata.csv convert -i Mouse load metadata_2.xls convert -i mouseID combine save output.txt

As above then add metadata into a region_sample_key.txt using column SampleName and SampleID as the merge on column
 keymaster.py load metadata.csv convert -i Mouse load metadata_2.xls convert -i mouseID combine add_metadata -k region_sample_key.txt SampleName save output.txt

Add fully formated metadata to sample key
 keymaster.py load key_with_metadata add_metadata -k region_sample_key.txt SampleName save output.txt

Note that you have to call convert or load a key you call add_metadata

Combine two fully formed qiime sample keys with metadata already present.
 keymaster.py load sample_key.txt load sample_key2.txt combine save output.txt

Drop columns from 2 tables, combine them into one table then convert them to qiime sample key
 keymaster.py load metadata.txt drop Header1 load metadata2.txt drop BadHeader combine convert save output.txt

Convert a column named Date with values like 4/5/14 to a column named Month with values like 4
 keymaster.py load metadata.csv rename Date Month modify '(\s+)/\d+/\d+' '\1' save output.txt
'''

def clean_df(df):
    def _clean(s):
        return s.apply(lambda x: x if pd.isnull(x) else str(x))

    return df.apply(_clean, axis=0)


def auto_read_table(fp, **kwargs):
    keywords = {'skip_blank_lines': True}
    keywords.update(kwargs)

    if isinstance(fp, pd.DataFrame):
        df = fp
    else:
        ext = Path(fp).ext
        if ext == '.xls' or ext == '.xlsx':
            df = pd.read_excel(fp, **keywords)
        elif ext == '.csv':
            df = pd.read_csv(fp, **keywords)
        elif ext == '.txt' or ext == '.tsv':
            df = pd.read_table(fp, **keywords)
        else:
            df = pd.read_table(fp, sep=None, **keywords)

    # drop columns and rows with all NAs
    no_na = df.dropna(axis=1, how='all').dropna(axis=0, how='all')

    # Convert all the non-NA values to real python strings
    # df.astype(str) does not ensure this
    return clean_df(no_na)


'''
Qiime requires that the header line starts with a # and is \t separated
Qiime requires that Description be the last column of key
'''


@gtac.util.temp_file(scalar=False, out=1)
def write_tmp_qiime_formatted_key(input_df, id_col):
    '''
    Adds columns named #SampleID and Description that are identical input_df[id_col]
    '''
    # Remove any / chars from headers as this causes problems down the line
    input_df.rename(columns=lambda x: x.replace('/', '-'), inplace=True)

    try:
        id_series = input_df.pop(id_col)
    except KeyError as e:
        logger.critical('no column named {0}. Columns in df are {1}'.format(
            id_col, ';'.join(input_df.columns)))

        raise e

    input_df.index = pd.Index(id_series.tolist(), name='#SampleID')
    input_df['BarcodeSequence'] = pd.np.nan
    input_df['LinkerPrimerSequence'] = pd.np.nan

    # Set Description column to ensure we have at least one non-empty column
    # This column is removed later in convert and replaced with cleaned qiime
    # sample names
    input_df['Description'] = id_series.tolist()

    #guess what qiime is going to output, this could break easily
    tmp_file = Path(tempfile.mkstemp(suffix='.txt')[1])
    tmp_qiime_corrected = tmp_file.replace('.txt', '_corrected.txt')
    tmp_qiime_log = tmp_file.replace('.txt', '.log')

    input_df.to_csv(
        tmp_file,
        sep='\t',
        header=True,
        index=True,
        na_rep='NA',
        encoding='utf-8')

    check_id_map.check_mapping_file(
        tmp_file,
        output_dir=tmp_file.dirname() + os.path.sep,  #bug in qiime
        verbose=False,
        disable_primer_check=True, )

    with open(tmp_qiime_log, 'r') as log:
        logger.warning(log.read())

    return tmp_qiime_corrected, tmp_file, tmp_qiime_log


def clean_qiime_sample(sample):
    return re.sub(r'\.{2,}', '.', str(sample)).strip('.')


def clean_qiime_category(category):
    return re.sub(r'[ -]+', '_', category).strip('_')


def combine(df_list):
    final_df = df_list[0]
    for df in df_list[1:]:
        final_df = final_df.combine_first(df)

    return final_df


def is_qiime_sample_key(df):
    return df.index.name == '#SampleID' and 'Description' in df

def _save(df, output):
    # Should only be used on qiime sample key dataframes
    #reorder Description column so it is at the end
    cols = df.columns.tolist()
    if 'Description' in cols:
        cols.remove('Description')
        cols.append('Description')

    df[cols].to_csv(
        output, sep='\t', index=True, na_rep='NA', encoding='utf-8')


@click.group(chain=True)
@click.option('--debug', is_flag=True)
@click.pass_context
def cli(ctx, debug):
    '''
    Table queue is LIFO
    '''
    if debug:
        import pdb; pdb.set_trace()

    logging.basicConfig()
    ctx.obj = list()


@cli.command()
@click.argument('column')
@click.pass_context
def add_metadata(ctx, column):
    '''
    Add metadata from keys that have different #SampleID naming than the OTU table to a
      key that has proper #SampleID naming and a column that shares values with the metadata #SampleID
    First key in queue is the base with column named `column`, all others are metadata keys.
    This command combines all keys and then clears queue
    '''
    key_df = ctx.obj[0]  # qiime sample key indexed with desired sample names
    df_list = []
    for meta_df in ctx.obj[1:]:
        # We get rid of common columns because otherwise duplicate columns
        # show up ie data_x and data_y
        left_cols = pd.Index(
            [column]).union(key_df.columns.difference(meta_df.columns))

        df_list.append(key_df[left_cols].merge(
            meta_df, left_on=column, right_index=True, how='left'))

    # Replace NA values in the merged key with original values in key_df
    df_list.append(key_df)
    final_df = combine(df_list)

    del ctx.obj[:]  # Clear the queue
    ctx.obj.append(final_df)


@cli.command()
@click.option(
    '--how', default='left', help='same as pandas.DataFrame.join kwarg')
@click.pass_context
def join(ctx, how):
    '''
    Join the last (right) sample key to the second to last (left) key by #SampleID values.
    All common columns are taken from the second to last (ie left) key.
    '''
    right_df, left_df = ctx.obj.pop(), ctx.obj.pop()
    right_cols = right_df.columns.difference(left_df.columns)
    final_df = left_df.join(right_df[right_cols], how=how)
    ctx.obj.append(final_df)


@cli.command('combine')
@click.pass_context
def cli_combine(ctx):
    '''
    Takes all keys in the queue and combines them into one.
    If any of the dataframes in the queue do not appear to be valid
    qiime sample keys, an error is thrown
    '''
    if len(ctx.obj) == 0:
        logger.warning('there are no dataframes in queue to combine')
    elif not all(is_qiime_sample_key(df) for df in ctx.obj):
        raise ValueError('not all dataframes in queue are qiime sample keys!')

    combined_df = combine(ctx.obj)
    del ctx.obj[:]  # Clear the queue
    ctx.obj.append(combined_df)


@cli.command('drop')
@click.argument('column', type=str)
@click.pass_context
def drop(ctx, column):
    '''
    Removes the column named 'column' from the in-memory dataframe
    '''
    df = ctx.obj.pop()
    df.drop(column, axis=1, inplace=True, errors='ignore')
    ctx.obj.append(df)


@cli.command('drop_redundant')
@click.pass_context
def drop_redundant(ctx):
    '''
    Removes columns for which all values are the same.
    This is needed to run Qiime group_significance script
    '''
    df = ctx.obj.pop()
    select = df.apply(lambda x: len(set(x)) > 1, axis=0)
    df = df.loc[:, select]
    ctx.obj.append(df)


@cli.command('add')
@click.argument('column', type=str)
@click.argument('value', type=str)
@click.pass_context
def add(ctx, column, value):
    '''
    Adds a new column named 'column' filled with string 'value'
    Works only on an in-memory dataframe
    '''
    df = ctx.obj.pop()
    df[column] = value
    ctx.obj.append(df)


@cli.command('load')
@click.argument(
    'table', type=click.Path(
        resolve_path=True, exists=True, dir_okay=False))
@click.option('-s', '--sheetname', type=str)
@click.pass_context
def load(ctx, table, sheetname):
    '''
    Loads a table into memory. If #SampleID and Description are columns
    the #SampleID column is converted into the index.
    Places the dataframe at the front of the queue
    Pass a name into the --sheetname option if loading an excel file
    '''
    options = {}
    if sheetname is not None:
        options['sheetname'] = sheetname

    df = auto_read_table(table, index_col=False, comment=None, **options)
    if '#SampleID' in df and 'Description' in df:
        df.set_index('#SampleID', inplace=True)
    ctx.obj.append(df)


@cli.command('modify')
@click.argument('column', type=str)
@click.argument('regex', type=str)
@click.argument('replacement', type=str)
@click.pass_context
def modify(ctx, column, regex, replacement):
    '''
    Takes a raw python regular expression to modify the data (not column name) of a column
    The replacement can use \1 \2 notation for advanced usage
    '''
    df = ctx.obj.pop()
    #only want to pass strings into re functions
    data = df[column].dropna()
    modified_data = data.apply(lambda x: re.sub(regex, replacement, str(x)))
    df[column].update(modified_data)
    ctx.obj.append(df)


@cli.command('rename')
@click.argument('column', type=str)
@click.argument('newcolumn', type=str)
@click.pass_context
def rename(ctx, column, newcolumn):
    '''
    Renames the df column named 'column' to 'newcolumn'
    '''
    df = ctx.obj.pop()
    df.rename(columns={column: newcolumn}, inplace=True)
    ctx.obj.append(df)


@cli.command('samples')
@click.argument('samples_fh', type=click.File('r'))
@click.option(
    '-r',
    '--regex',
    type=str,
    help='regexp string with capture groups to parse a sample name for metadata info'
)
@click.option(
    '-h',
    '--header',
    type=str,
    help='comma separated header list for metadata')
@click.pass_context
def samples(ctx, samples_fh, regex, header):
    '''
    Takes a list of sample names and generates a metadata table
    based off of tokens separated by a delimiter character
    '''
    full_header = ['sample']
    if header:
        full_header.extend(header.split(','))
    data = []
    for sample in samples_fh.readlines():
        s = sample.strip()
        d = [s]
        if regex:
            d.extend(re.search(regex, s).groups())
        data.append(d)
    df = pd.DataFrame.from_records(data, columns=full_header)

    ctx.obj.append(clean_df(df))


@cli.command()
@click.argument(
    'output',
    required=True,
    type=click.Path(
        resolve_path=True, dir_okay=False))
@click.pass_context
def save(ctx, output):
    key_df = ctx.obj.pop()
    _save(key_df, output)

@cli.command()
@click.argument('excel', required=True, type=click.Path(resolve_path=True, dir_okay=False, exists=True))
@click.argument('output_dir', required=True, type=click.Path(resolve_path=True, dir_okay=True, file_okay=False))
def split(excel, output_dir):
    '''
    Takes an excel file and writes out every sheet as a separate
    table in output_dir named after the sheet name
    '''
    df_dict = pd.read_excel(excel, sheetname=None)
    output_dir = Path(output_dir).mkdir_p()
    for name, df in df_dict.items():
        fmt_df = auto_read_table(df)
        n = re.sub(r'\s+', '_', name)
        fp = output_dir.joinpath(n + '.tsv')
        fmt_df.to_csv(fp, sep='\t', index=False)


@cli.command()
@click.option('-i', '--id_col', type=str)
@click.pass_context
def convert(ctx, id_col):
    '''
    Convert a customer provided metadata table into a qiime ready sample key
    You must first edit the input in excel to make it a normal table
    To-do use fuzzy matching for the id column name.
    '''
    df = ctx.obj.pop()
    with write_tmp_qiime_formatted_key(df, id_col) as tmp_mapping:
        qiime_key_df = auto_read_table(tmp_mapping, index_col=False)
        qiime_key_df.set_index('#SampleID', inplace=True)
        qiime_key_df.rename(columns=clean_qiime_category, index=clean_qiime_sample, inplace=True)

    qiime_key_df[
        'Description'] = qiime_key_df.index  # qiime complains without it
    ctx.obj.append(qiime_key_df)


if __name__ == '__main__':
    cli()
