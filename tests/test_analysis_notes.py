from builtins import next
import pytest
from gtac.analysis_notes import *


def strip_i5(results):
    for k, v in results.items():
        for e in v:
            e[1] = e[1].split(Index.dual_joiner)[0]
    return results


def record1():
    data = '''@@1923_{}_human_chip-seq
Lane 1: BC17_9124IL3_2,BC07_NSIL3_2, Lane 2: mi9_WT_2,
BC01_NS_1 TCGCCTTA  BC02_NS_2 CTAGTACG  BC06_NSIL3_1 CATGCCTA  BC07_NSIL3_2 GTAGAGAG BC12_9124_1 TCCTCTAC  BC13_9124_2 ATCACGAC  BC16_9124IL3_1 ACAAACGG  BC17_9124IL3_2 ACCCAGCA  NS8_WT_1 TCGCCTTA  NS9_WT_2 CTAGTACG  mi8_WT_1 AGGAGTCC  mi9_WT_2 CATGCCTA
'''

    results = {
        '1': [[RawIndex.name, 'ACCCAGCA', 'BC17_9124IL3_2'],
              [RawIndex.name, 'GTAGAGAG', 'BC07_NSIL3_2']],
        '2': [[RawIndex.name, 'CATGCCTA', 'mi9_WT_2']]
    }

    return data, results


def record2():
    data = '''@@1917_{}_human_alignment
Lane 6: Donor1 CD56L K27ac, Lane 7: Donor2 CD56L K27ac, 
Donor1 CD56L K27ac: Truseq 1, Donor1 CD56L K4me3: Truseq 2, Donor1 CD56L Input: Truseq 3, Donor1 CD56H K27ac: Truseq 4, Donor1 CD56H K4me3: Truseq 5, Donor1 CD56H Input: Truseq 6, Donor2 CD56L K27ac: Truseq 7, Donor2 CD56L K4me3: Truseq 8, Donor2 CD56L Input: Truseq 9, Donor2 CD56H K27ac: Truseq 10, Donor2 CD56H K4me3: Truseq 11, Donor2 CD56H Input: Truseq 12, 
@@1917_{}_oltz_human_demux
Notes: Lane 3: 57, 58, 59, 60; Lane 8: 61, 62, 63, 64; Lane 4: 65, 66, 67, 68; Lane 5: 69, 70, 71
57: Truseq 2, 58: Truseq 4, 59: Truseq 5, 60: Truseq 6, 61: Truseq 8, 62: Truseq 7, 63: Truseq 1, 64: Truseq 3, 65: Truseq 9, 66: Truseq 10, 67: Truseq 11, 68: Truseq 12, 69: Truseq 1, 70: Truseq 2, 71: Truseq 3
'''

    results = {
        '6': [['truseq1', TruSeqIndex._seqs['truseq1'], 'Donor1 CD56L K27ac']],
        '7': [['truseq7', TruSeqIndex._seqs['truseq7'], 'Donor2 CD56L K27ac']],
    }
    return data, results


def record3():
    data = '''Index3 ATGGCA Index4 CTGCGTA
@@2011_{}_alignment
Lane1: 50, Lane2: 66, 70
Index3 50 Index4 66 TruSeq10 70
'''

    results = {
        '1': [['index3', 'ATGGCA', '50']],
        '2': [['index4', 'CTGCGTA', '66'],
              ['truseq10', TruSeqIndex._seqs['truseq10'], '70']]
    }

    return data, results


def record4():
    i5 = 'GCGTAAGA'
    data = '''@@1961_{}_alignment
Lane2 146, 119, 7
Dual Indexes Illumina-i7 146 N701 TAAGGCGA 119 N702 CGTACTAG 7 ATCAG Illumina-i5 146 N502, S502, E502 CTCTCTAT
'''

    results = {
        '2': [
            [RawIndex.name, 'TAAGGCGA-CTCTCTAT', '146'],
            [RawIndex.name, 'CGTACTAG-' + i5, '119'],
            [RawIndex.name, 'ATCAG-' + i5, '7'],
        ]
    }
    return data, results, i5


def record5():
    data = '''@@2074_2_alignment
IL1+23 Mnk3, IL1-4 lane 2
IL1+23 Mnk3 N705 GGACTCCT IL1-4 N706 TAGGCATG
'''

    results = {
        '1': [[RawIndex.name, 'GGACTCCT', 'IL1+23 Mnk3'],
              [RawIndex.name, 'TAGGCATG', 'IL1-4']]
    }
    return data, results


def record6():

    results = {
        '1': [['index3', 'ATGGCA', '50']],
        '2': [['index4', 'CTGCGTA', '66'],
              ['truseq5', TruSeqIndex._seqs['truseq5'], '70']]
    }

    return data, results


@pytest.mark.parametrize('raw,index', [
    ('''@@1923_{}
Lane 1: sample1
sample1 ATCGTG
    ''', None),
    ('''@@1923_{}
Lane 1: sample1
sample1 ATCGTG''', None),
    ('''MyIndex ATTGCG
@@1923_{}
Lane 1: sample1
sample1 ATCGTG''', 'MyIndex ATTGCG'),
])
def test_record_parse(raw, index):
    rec = next(Record.parse(raw))
    assert rec.template == '1923_{}'
    assert rec.samples == 'Lane 1: sample1'
    assert rec.pairing == 'sample1 ATCGTG'
    assert rec.index == index


def test_token_failure():
    with pytest.raises(ValueError):
        Index.factory('null///?')


@pytest.mark.parametrize(
    'line', ['MyIndex, ATGCC', '-,MyIndex, ATGCC', 'ATGCC MyIndex'])
def test_make_index(line):
    cls = make_index_subclass(line)
    assert cls._seqs == {'myindex': 'ATGCC'}
    assert cls in Index.__subclasses__()


@pytest.mark.parametrize('data,results', [record1(), record2(), record5()])
def test_process_record(data, results):
    '''
    Can fail if u pass --pdb or -s to py.test for some reason...
    The problem is the Index subclass registration
    '''
    record = next(Record.parse(data))
    x = process_record(record)
    assert x == results


@pytest.mark.parametrize('data,results,i5', [record4()])
def test_process_record_w_i5(data, results, i5):
    record = next(Record.parse(data))
    x = process_record(record, i5=i5)
    assert x == results


@pytest.mark.parametrize('data,results,i5', [record4()])
def test_process_record_no_dual(data, results, i5):
    strip_i5(results)
    record = next(Record.parse(data))
    x = process_record(record, no_dual=True)
    assert x == results


@pytest.mark.parametrize('data,results,i5', [record4()])
def test_process_record_w_i5_no_dual(data, results, i5):
    strip_i5(results)
    record = next(Record.parse(data))
    x = process_record(record, i5=i5, no_dual=True)
    assert x == results


def test_unknown_truseq():
    data = '''@@2011_{}_alignment
Lane1: 50, Lane2: 66, 70
TruSeq3 50 TruSeq333 66 TruSeq5 70
'''
    record = next(Record.parse(data))
    with pytest.raises(ValueError):
        process_record(record)
