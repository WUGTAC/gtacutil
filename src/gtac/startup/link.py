from builtins import range, object
import os
import csv
import re
import logging

from path import Path

from gtac.startup.fetch_files import fetch_files, FetchItem

# FetchItem = namedtuple('FetchItem', 'target,link')

logger = logging.getLogger(__name__)


def clean_sample_name(name):
    '''
    All raw sample names should be run through this function before
    being processed by a program. Weird characters cause weird errors
    '''
    no_bad_chars = re.sub(r'[^a-zA-Z0-9_.]', '_', name.strip())
    no_false_joins = re.sub(r'_[.]_', '.', no_bad_chars)
    return re.sub(r'_{2,}', '_', no_false_joins)


class SampleTableReader(object):
    def __init__(self, sample_table_fh):
        self.reader = csv.DictReader(
            sample_table_fh.readlines(),
            delimiter='\t', )

    def __iter__(self):
        return (self._parse(line) for line in self.reader)

    def _parse(self, line):
        ret = []
        for i in '12':
            # Path(arg) will raise TypeError if arg is None
            try:
                f = FetchItem(
                    link=Path(line.get('link' + i)),
                    target=Path(line.get('read' + i)),
                    metadata={'sample': line.get('sample')}, )
            except TypeError:
                pass
            else:
                ret.append(f)

        if not ret:
            raise ValueError(
                'line does not appear to be sample table row: %s' % line)
        return ret


def get_fetch_items(samples, raise_errors=True):
    '''
    Reads samples.txt and returns list of FetchItem tuples
    If there are multiple targets matching to a single link this
    must be handled properly by the fetch function.
    Check that targets exist and that links can be made?
    We could add safety to make sure this never runs when any link exists
    already
    '''

    try:
        reader = SampleTableReader(samples)
    except AttributeError:
        with open(samples, 'r') as fh:
            reader = SampleTableReader(fh)

    fetch_items = []
    for items in reader:
        fetch_items.extend(items)

    errors = []
    for each in fetch_items:
        if each.link.exists():
            logger.warning('link filepath already exists: %s' % each.link)
        elif not each.target.exists():
            errors.append('target file does not exist: %s' % each.target)
        elif not each.target.access(os.R_OK):
            errors.append('target file is not readable: %s' % each.target)
        else:
            pass

    for _dir in set(e.link.parent for e in fetch_items):
        if not _dir.access(os.W_OK):
            errors.append(
                'parent directory of link is not writable: %s' % _dir)

    if errors and raise_errors:
        raise IOError('\n'.join(errors))

    return fetch_items


def fetch_fastq(sample_table, fetch_method='link'):
    '''
    Assumes that the analysis directory and any subdirectories already exist

    :param str sample_table: Path to table with sample names to reads
    :param str fetch_method: Method to use for fetching reads.
    :return: List of created file-paths
    :raises IOError: if sample_table file does not exist or fetching is impossible
    :raises KeyError: if fetch_method is not recognized
    '''
    # Start of the work of the function
    if not os.path.exists(sample_table):
        raise IOError('The sample table %s does not exist' % sample_table)

    fetch_items = get_fetch_items(sample_table)
    return fetch_files(fetch_items, fetch_method=fetch_method)


def write_sample_table(seq_data_list, sample_table, make_link_name=None):
    '''
    Automatically creates samples.txt from list of SequenceData objects
    This file is a tab-separated table mapping a sample name to one or two
    fastq files in a demux directory
    The header is : sample, read1, link1 [,read2, link2]

    :param list seq_data_list: List of SequenceData objects
    :param str sample_table: path to write samples table
    :param function make_link_name: function with signature f(SequenceData, str, int)
    :raises IOError: if one sample within a lane has different number than others in the lane.
    '''
    line_list = []
    for seq_data in seq_data_list:
        _dict = seq_data.get_sample_to_fastq()
        for sample, reads in _dict.items():
            clean_sample = clean_sample_name(sample)
            line = [clean_sample]
            for i in range(len(reads)):
                line.extend(
                    [reads[i],
                     make_link_name(seq_data, clean_sample, i + 1)])
            line_list.append(line)

        if not all(len(e) == len(line_list[0]) for e in line_list):
            # maybe make this a warning so it can be caught and full samples
            # table investigated
            raise IOError('unequal number of reads in samples' % seq_data)

        with open(sample_table, 'w') as fh:
            writer = csv.writer(fh, delimiter='\t')
            header = ['sample', 'read1', 'link1']
            if max(len(e) for e in line_list) >= 5:
                header += ['read2', 'link2']
            writer.writerow(header)
            for line in line_list:
                writer.writerow(line)
