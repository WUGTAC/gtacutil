import pytest
import subprocess
from gtac.util import run_command


@pytest.fixture
def table(tmpdir):
    table = '''sample,color,count,type
a,blue,5,x
b,green,6,y
'''
    dir_ = tmpdir.mkdir('sub')
    fp = dir_.join('test.csv')
    fp.write(table)
    return fp, None


@pytest.fixture
def metadata(tmpdir):
    table = '''Variable,count,bool
x,1,T
z,20,T
'''
    dir_ = tmpdir.join('sub')
    fp = dir_.join('metadata.csv')
    fp.write(table)
    return fp, None


@pytest.fixture
def sample_list(tmpdir):
    table = '''1_a
2_b
3_c
'''
    answer = '''#SampleID,Number,String,Description
1.a,1,a,1.a
2.b,2,b,2.b
3.c,3,c,3.c
'''
    answer = answer.replace(',', '\t')
    options = '-d _ -h Number,String'
    fp = tmpdir.join('metadata.csv')
    fp.write(table)
    return fp, answer, options


def test_general(table):
    fp, _ = table
    out = fp.dirpath().join('result.txt')
    cmd = '''keymaster load {fp} drop type add newtype bar \
rename color eyecolor convert -i sample save {out}
'''.format(
        fp=fp, out=out)

    subprocess.check_output(cmd.split())
    answer = '''#SampleID,eyecolor,count,newtype,Description
a,blue,5,bar,a
b,green,6,bar,b
'''
    assert out.read().replace('\t', ',') == answer


def test_add_metadata(table, metadata):
    meta, _ = metadata
    fp, _ = table
    out = fp.dirpath().join('result.txt')

    cmd = '''keymaster load {fp} convert -i sample \
load {meta} convert -i Variable add_metadata type save {out}'''.format(
        **locals())

    subprocess.check_output(cmd.split())

    # The combine() call in add_metadata alphabetizes the columns
    answer = '''#SampleID,bool,color,count,type,Description
a,T,blue,1,x,x
b,NA,green,6,y,b
'''
    assert out.read().replace('\t', ',') == answer


def test_samples(sample_list):
    fp, answer, options = sample_list
    out = fp.dirpath().join('result.txt')
    cmd = '''keymaster samples {options} {fp} \
convert -i sample save {out}'''.format(**locals())
    p = run_command(cmd)
    assert p.returncode == 0
    assert out.read() == answer
