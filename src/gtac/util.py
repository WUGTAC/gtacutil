from __future__ import print_function
from builtins import str, input
from past.builtins import basestring
from path import Path
from glob import glob
from subprocess import Popen, PIPE
import atexit
import re
import shutil
import functools
import contextlib
import tempfile
import random
import gzip
import os
import sys
import logging
import collections


@contextlib.contextmanager
def cd(Path):
    wd = os.getcwd()
    os.chdir(Path)
    yield None
    os.chdir(wd)


def get_script_dir():
    '''
    Warning: this will not work correctly if used in imported file
    '''
    return Path(sys.argv[0]).dirname()


def get_unique_file(glob_pattern, empty=True):
    files = glob(glob_pattern)
    if empty and len(files) == 0:
        return None

    if len(files) > 1:
        raise Exception(
            'The glob pattern %s returned more than one file : %s' %
            (glob_pattern, ' '.join(files)))
    elif len(files) == 0:
        raise Exception(
            'The glob pattern %s did not return any files' % glob_pattern)
    else:
        return files[0]


def list_prompt(lst):
    if not (sys.stdin.isatty() and sys.stdout.isatty()):
        raise OSError('stdout or stdin not connected to tty')
    for i, x in enumerate(lst):
        print('{} : {}'.format(i, x))
    while True:
        index = int(input("Choose item by typing the number: ").strip())
        if index < 0 or index >= len(lst):
            continue
        else:
            return lst[index]


def make_unique_file(template):
    base = Path(str(template).rstrip(os.path.sep))
    fp = base
    i = 1
    while fp.exists():
        fp = base + '.' + str(i)
        i += 1
    return fp


def memoize(obj):
    cache = obj.cache = {}

    @functools.wraps(obj)
    def memoizer(*args, **kwargs):
        if args not in cache:
            cache[args] = obj(*args, **kwargs)
            return cache[args]
        else:
            return cache[args]

    return memoizer


def regexp_glob(_dir, pat):
    if isinstance(pat, basestring):
        pat = re.compile(pat)
    ret = []
    for fp in _dir.glob('*'):
        m = pat.search(fp.basename())
        if m:
            ret.append(fp)
    return ret


ProcessOutput = collections.namedtuple('ProcessOutput',
                                       'stdout,stderr,cmd,returncode')


def run_command(cmd, input=None, **kwargs):
    '''
    cmd can be list or string. If string it gets split on whitespace
    input is a str that is piped into cmd's stdin
    **kwargs is passed into Popen
    '''
    # this universal_newlines triggers python3 text mode so we use for compatibility
    defaults = {'universal_newlines': True}
    kwargs.update(defaults)

    logger = logging.getLogger(__name__)
    if isinstance(cmd, basestring):
        cmd = cmd.split()

    logger.debug(str(cmd))
    if input is not None:
        if not isinstance(input, basestring):
            raise TypeError('input must be a str not %s', type(input))
        else:
            kwargs['stdin'] = PIPE
    p = Popen(cmd, stdout=PIPE, stderr=PIPE, **kwargs)
    streams = p.communicate(input=input)
    return ProcessOutput(
        stdout=streams[0], stderr=streams[1], cmd=cmd, returncode=p.returncode)


def _get_open(fp):
    fp = Path(fp)
    if fp.ext == '.gz':
        return gzip.open
    else:
        return open


@contextlib.contextmanager
def smart_open(fp, mode='r'):
    fh = _get_open(fp)(fp, mode)
    try:
        yield fh
    finally:
        fh.close()


@contextlib.contextmanager
def atomic_write(fp, mode='w'):
    if 'w' not in mode:
        raise TypeError('Invalid mode, only use write modes here: %s' % mode)
    # Open here to make sure final destination is writable
    fh_final = open(fp, mode='wb')
    tmp = tempfile.mkstemp()[1]
    atexit.register(os.unlink, tmp)
    fh = _get_open(fp)(tmp, mode)
    try:
        yield fh
    finally:
        fh.close()
        p = Popen(['cat', tmp], stdout=fh_final)
        p.wait()
        fh_final.close()


def temp_file(scalar=True, out=None):
    #If out is more than 1 then the output from the
    # wrapped function must not be a scalar

    #wrap a scalar as a tuple
    if scalar:
        call = lambda x: (x, )
    else:
        call = lambda x: x

    #unpack a single return value to scalar
    #if no specific number is passed to out, return everything
    if out > 1:
        process = lambda x: x[:out]

    elif out == 1:
        process = lambda x: x[0]

    elif out is None and scalar:
        process = lambda x: x[0]

    elif out is None and not scalar:
        process = lambda x: x

    else:
        assert False

    def check_type(ret, type_):
        return all([isinstance(x, type_) for x in ret])

    def decorator(func):
        '''
        Decorator to return filesystem names of temporary objects
        Meant to be used in a with block. temp files are
        deleted when with block exits.
        Decorated function should return the path to temp file (single or iter)
         or a NamedTemporaryFile object
        '''

        @contextlib.contextmanager
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            og_ret = call(func(*args, **kwargs))

            if check_type(og_ret, tempfile._TemporaryFileWrapper):
                destroy = lambda x: x.close()
                ret = og_ret

            elif check_type(og_ret, basestring):
                destroy = lambda x: x.unlink_p()
                ret = [Path(x)
                       for x in og_ret]  #cast all strings as path objects

            else:
                assert False

            try:
                yield process(ret)
            finally:
                for each in ret:
                    destroy(each)

        return wrapper

    return decorator


def weighted_choice(choices):
    total = sum(w for c, w in choices)
    r = random.uniform(0, total)
    upto = 0
    for c, w in choices:
        if upto + w > r:
            return c
        upto += w
    assert False, "Shouldn't get here"
