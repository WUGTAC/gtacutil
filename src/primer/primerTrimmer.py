#!/usr/bin/env python
from __future__ import division
from future import standard_library
standard_library.install_aliases()
from builtins import zip
from builtins import next
from builtins import str
from builtins import range
from builtins import object
from past.utils import old_div
import argparse
import functools
import tempfile
import logging
import logging.config
import atexit
import pickle as pickle
from itertools import combinations

from pkg_resources import resource_string
import yaml
import Levenshtein
import sh
import pandas as pd
import regex
import attr
from path import Path
from Bio import SeqIO

import primer
from gtac.util import smart_open, atomic_write
from gtac.cmdline import non_negative_int

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler)


def find_read_length(fastq_fp):
    with smart_open(fastq_fp, 'rt') as fastq_handle:
        read_length = len(next(SeqIO.parse(fastq_handle, "fastq")))

    assert read_length > 0
    return read_length


def find_adapter_contamination_prior(primer_df, read_length):

    if any(pd.isnull(primer_df['size'])):
        return None
    else:
        short = (primer_df['size'].astype(int) < read_length).sum()
        total = max(1.0, float(len(primer_df)))
        return min(old_div(short, total), 0.99)  #scythe doesn't like prior=1


def get_default_untrimmed_fp(output_fp_list):
    subber = regex.compile(r'\.f(?:ast)?q(?:\.gz)?$', flags=regex.I)
    fn = lambda m: '_no_trim' + m.group(0)
    return [Path(subber.sub(fn, fp)) for fp in output_fp_list]


def get_trim_parser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        '-i',
        '--input',
        nargs=2,
        type=Path,
        required=True, )

    parser.add_argument('-o', '--output', nargs=2, type=Path, required=True)

    parser.add_argument(
        '-p', '--primers', type=Path, required=True,
        help='tab separated file with no column headers of form '+\
        'name primer_one_seq primer_two_seq amplicon_length'
        )

    parser.add_argument(
        '-a', '--adapters', type=Path,
        help='fasta file containing adapters to trim. If this is not passed, the reads '+\
        'are assumed to have no adapter contamination and scythe will not be called'
        )

    parser.add_argument(
        '-u', '--untrimmed', type=Path, nargs=2,
        help='filepaths for fastq records for which trimming fails. '+\
        'The default is to append _no_trim to the output filepaths.'
        )

    parser.add_argument(
        '--pickle', type=Path,
        help='path to pickle a dict fastq.id '+\
        '(ie everything up to space) to amplicon_name'
        )

    parser.add_argument(
        '--name',
        action='store_true',
        help='adds string amplicon={amplicon_name} to output fastq record names'
    )

    parser.add_argument(
        '-t',
        '--extra_trim',
        type=non_negative_int,
        default=1,
        help='number of extra bases to trim after primers')

    parser.add_argument(
        '-m', '--max_errors', type=non_negative_int,
        help='max number of errors (mismatches or indels) allowed for '+\
        'matching a 5 prime primer to an amplicon'
        )

    parser.add_argument(
        '-q',
        '--max_mm_qual',
        type=non_negative_int,
        default=20,
        help=
        'max sum of mismatched base qualities for primer matching on 3 prime end'
    )

    parser.add_argument(
        '-x',
        '--min_match_len',
        type=non_negative_int,
        default=6,
        help=
        'min number of bases on 3 prime end that must match for primer trimming'
    )

    return parser


def maximum_safe_errors(primer_tuple_lst):
    '''
    Returns None if primer_tuple_lst has one or fewer entries
    (ie no valid primer pair combinations)
    '''

    def trim_pair(pair):
        N = min(len(e) for e in pair)
        return [e[:N] for e in pair]

    min_, closest_combo = float('inf'), None
    for combo in combinations(primer_tuple_lst, 2):
        combo_max = -1
        for pair in zip(*combo):
            combo_max = max(combo_max, Levenshtein.distance(*trim_pair(pair)))
        if combo_max > -1 and combo_max < min_:
            min_, closest_combo = combo_max, combo

    assert min_ >= 0

    if min_ == float('inf'):
        safe = None
    else:
        safe = old_div(abs(min_ - 1), 2)
    logger.info(
        'Min lev distance: %s; Closest combo: %s; Safe number of error: %s;',
        min_, closest_combo, safe)
    return safe


def reverse_compliment(seq):
    return ''.join(primer.REV_BASE.get(e) for e in seq.upper()[::-1])


def read_gtac_primer_file(fp, errors=0):

    if isinstance(errors, int):
        assert errors >= 0, 'error number must be non-negative, was passed %s' % errors

    primer_df = \
      pd.read_table(
          fp,
          sep='\t',
          names=['name', 'p1', 'p2', 'size'],
          dtype=pd.np.dtype('O')
          )
    return primer_df


def run_adapter_trim_subprocess(primer_df, adapter_fp, input_fastq_lst):
    '''
    Assumes that all reads in input fastq files are same length and that
    both input fastqs have same length reads
    Returns filepath of named temp files that contain output of adapter trimming
    '''
    DEFAULT_PRIOR = 0.4
    TRIM_CMD = sh.Command('scythe')

    logger.info('Running adapter trimming procedure with command %s', TRIM_CMD)

    read_length_lst = [find_read_length(f) for f in input_fastq_lst]
    logger.info('Read length of files %s are %s', input_fastq_lst,
                read_length_lst)

    prior = find_adapter_contamination_prior(primer_df, read_length_lst[0])
    if prior is None:
        prior = DEFAULT_PRIOR

        logger.info(
            'amplicon size info missing from primer_df, prior defaulting to %s',
            DEFAULT_PRIOR)

    file_pairs = [(fp_in, tempfile.mkstemp(suffix='.fastq')[1])
                  for fp_in in input_fastq_lst]

    tmp_path_tuple = tuple(Path(e[1]) for e in file_pairs)

    #Remove the temp files when program exits
    def _cleanup(path_list):
        for p in path_list:
            p.remove_p()

    atexit.register(_cleanup, tmp_path_tuple)

    for fp_in, tmp in file_pairs:
        TRIM_CMD(
            fp_in,
            a=adapter_fp,
            o=tmp,
            p='{:.2f}'.format(prior), )

    return tmp_path_tuple


def construct_amplicons(primer_df, errors=0):
    DEFAULT_ERRORS = 2
    # Uppercase all primers and drop duplicate primer pairs
    no_dups_df = primer_df.applymap(lambda x: x.upper()).drop_duplicates(
        ['p1', 'p2'])

    # Figure out how many errors to use for the primer binding
    max_errors = maximum_safe_errors(no_dups_df.loc[:, ['p1', 'p2']].values)
    if errors is None and max_errors is None:
        # This should only occur if panel only contains one or fewer amplicons
        assert len(no_dups_df) <= 1
        logger.info(
            'No error number passed and ambiguous result from maximum_safe_errors'
        )
        primer_errors = DEFAULT_ERRORS
    elif errors is None:
        logger.info(
            'error number passed is None, setting to max safe value %s',
            max_errors)
        primer_errors = max_errors
    elif max_errors is not None and max_errors < errors:
        logger.warning(
            'Passed primer error number %s is above safety, setting to %s',
            errors, max_errors)
        primer_errors = max_errors
    else:
        primer_errors = errors

    # Construct the primer and amplicon objects to be used for processing reads
    assert isinstance(primer_errors, int)
    amplicon_lst = []
    for each in no_dups_df.to_dict('records'):
        primers = [
            RegexpPrimer.from_seq(each[p], errors=primer_errors)
            for p in ['p1', 'p2']
        ]
        revcomp_primers = [
            RegexpPrimer.from_seq(
                reverse_compliment(each[p]), errors=primer_errors)
            for p in ['p1', 'p2']
        ]
        amplicon = FluidigmFastqAmplicon(
            name=each['name'],
            primers=primers,
            revcomp_primers=revcomp_primers)
        amplicon_lst.append(amplicon)
    return amplicon_lst


@attr.s
class RegexpPrimer(primer.Primer):
    regexp = attr.ib()
    errors = attr.ib()

    def bind(self, target_seq):
        m = self.regexp.match(target_seq)
        if m:
            yield m.span()

    @classmethod
    def from_seq(cls, seq, errors=0):
        char_set = [
            '[{}]'.format(''.join(primer.IUPAC_MAP[nt])) for nt in seq.upper()
        ]
        regexp = ''.join(char_set)
        final_regexp = '(?:%s){e<=%s}' % (regexp, errors)
        compiled = regex.compile(final_regexp)
        return cls(seq=seq, regexp=compiled, errors=errors)


@attr.s
class FluidigmFastqAmplicon(primer.Amplicon):
    def amplify(self, seq_tuple):
        for fwd, rev in [
                self._primer_pairs(),
                self._primer_pairs(reverse=True)
        ]:
            product = self._amplify(fwd, seq_tuple)
            if product:
                return AmplifyResults(
                    name=self.name, product=product, three_prime_seqs=rev)
        return AmplifyResults.empty()

    def _amplify(self, primers, seq_tuple):
        matches = []
        for p, seq in zip(primers, seq_tuple):
            m = next(p.bind(str(seq.seq)), None)
            if m:
                matches.append(seq[m[1]:])
            else:
                return None
        return matches

    def _primer_pairs(self, reverse=False):
        if reverse:
            return [self.primers[::-1], self.revcomp_primers]
        else:
            return [self.primers, self.revcomp_primers[::-1]]


@attr.s
class AmplifyResults(object):
    name = attr.ib()
    product = attr.ib()
    three_prime_seqs = attr.ib()

    @classmethod
    def empty(cls, product=None):
        return AmplifyResults(
            name=None, product=product, three_prime_seqs=None)

    def __bool__(self):
        return self.name is not None

    __nonzero__ = __bool__


@attr.s
class PrimerTrimmer(object):
    iupac_map = attr.ib()
    extra_trim = attr.ib(default=1)
    min_match_len = attr.ib(default=5)
    max_mm_qual = attr.ib(default=20)

    def trim(self, amplifyResult):
        results = []
        for seq, rev_p in zip(amplifyResult.product,
                              amplifyResult.three_prime_seqs):
            three_prime_trim = self._iupac_trim_right_end_with_quals(
                seq, rev_p.seq, self.min_match_len, self.max_mm_qual)
            # if no three prime match found, dont extra trim
            if three_prime_trim is seq:
                end = len(three_prime_trim)
            else:
                end = len(three_prime_trim) - self.extra_trim
            results.append(three_prime_trim[self.extra_trim:end])

        return results

    def _iupac_trim_right_end_with_quals(self, seq, pat, min_match_len,
                                         max_mm_qual):
        seqlen = len(seq)
        patlen = len(pat)
        minlen = min(seqlen, patlen)
        match = 0
        for i in range(min_match_len, minlen + 1):
            sum_mm_qual = 0
            for j in range(i):
                if str(seq[seqlen - i + j]) not in self.iupac_map[pat[j]]:
                    sum_mm_qual += seq.letter_annotations['phred_quality'][
                        seqlen - i + j]
                    if sum_mm_qual > max_mm_qual:
                        break
            else:
                match = i
        return seq[:seqlen - match] if match > 0 else seq


def memoize_subseq_size(amplicon_lst):
    max_size = 0
    max_error = 0
    for amp in amplicon_lst:
        p_sizes = (len(p) for p in amp.primers)
        max_size = max(max_size, max(p_sizes))

        p_errors = (p.errors for p in amp.primers)
        max_error = max(max_error, max(p_errors))
    return max_size + max_error


def build_memoize_identify(amplicon_lst):
    N = memoize_subseq_size(amplicon_lst)
    amplicon_dict = {a.name: a for a in amplicon_lst}

    def memoize_identify(obj):
        cache = obj.cache = {}

        @functools.wraps(obj)
        def memoizer(amplicons, read_pair):
            subseqs = tuple(str(e.seq[:N]) for e in read_pair)
            if subseqs not in cache:
                amplifyResults = obj(amplicons, read_pair)
                cache[subseqs] = amplicon_dict.get(amplifyResults.name)
                return amplifyResults
            else:
                amp = cache[subseqs]
                if amp is None:
                    return AmplifyResults.empty(read_pair)
                return amp.amplify(read_pair)

        return memoizer

    return memoize_identify


def identify_origin_amplicon(amplicons, read_pair):
    prev_results = AmplifyResults.empty(read_pair)
    for amp in amplicons:
        results = amp.amplify(read_pair)
        # We have found more than one matching amplicon, return empty
        if results and prev_results:
            logger.critical('Multiple amplicon matches detected: %s',
                            read_pair)
            return AmplifyResults.empty(read_pair)
        elif results:
            prev_results = results
    return prev_results


def trim_primers(trimmer, amplifyResults):
    if amplifyResults:
        trimmed_reads = trimmer.trim(amplifyResults)
        return AmplifyResults(
            name=amplifyResults.name,
            product=trimmed_reads,
            three_prime_seqs=amplifyResults.three_prime_seqs)
    else:
        return amplifyResults


def run_primer_trimming(fastq_fh_lst, amplicon_lst=None, primerTrimmer=None):
    input_record_pairs = zip(
        * [SeqIO.parse(fh, format='fastq') for fh in fastq_fh_lst])

    memoize_identify = build_memoize_identify(amplicon_lst)
    identify_fn = memoize_identify(identify_origin_amplicon)

    identified = (identify_fn(amplicon_lst, pair)
                  for pair in input_record_pairs)
    trimmed = (trim_primers(primerTrimmer, result) for result in identified)
    return trimmed


def main():
    try:
        logging_str = resource_string('primer', 'data/logging.yaml')
    except IOError as e:
        logging.warning(str(e))
    else:
        log_dict = yaml.safe_load(logging_str)
        logging.config.dictConfig(log_dict)

    parser = get_trim_parser()
    args = parser.parse_args()

    error_fp_list = \
      get_default_untrimmed_fp(args.output) \
      if args.untrimmed is None else args.untrimmed

    primer_df = read_gtac_primer_file(args.primers)
    assert len(
        primer_df) > 0, 'Primer file %s is empty, aborting' % args.primer

    #turn off call to scythe adapter trimmer if no adapter file passed
    if args.adapters is not None:
        adapter_trim_one_fp, adapter_trim_two_fp = \
          run_adapter_trim_subprocess(primer_df, args.adapters, args.input)
    else:
        adapter_trim_one_fp, adapter_trim_two_fp = args.input

    amplicon_lst = construct_amplicons(primer_df, errors=args.max_errors)
    primerTrimmer = PrimerTrimmer(
        iupac_map=primer.IUPAC_MAP,
        extra_trim=args.extra_trim,
        max_mm_qual=args.max_mm_qual,
        min_match_len=args.min_match_len)

    with smart_open(adapter_trim_one_fp, 'rt') as fh_in_1, \
      smart_open(adapter_trim_two_fp, 'rt') as fh_in_2, \
      atomic_write(args.output[0], 'wt') as fh_out_1, \
      atomic_write(args.output[1], 'wt') as fh_out_2, \
      atomic_write(error_fp_list[0], 'wt') as fh_err_1, \
      atomic_write(error_fp_list[1], 'wt') as fh_err_2:

        trimmed = run_primer_trimming(
            [fh_in_1, fh_in_2],
            amplicon_lst=amplicon_lst,
            primerTrimmer=primerTrimmer)
        read_name_to_amplicon = {}

        for result in trimmed:
            prod = result.product
            read_name = prod[0].id
            if result:
                SeqIO.write(prod[:1], fh_out_1, "fastq")
                SeqIO.write(prod[1:], fh_out_2, "fastq")
                read_name_to_amplicon[read_name] = result.name
            else:
                SeqIO.write(prod[:1], fh_err_1, "fastq")
                SeqIO.write(prod[1:], fh_err_2, "fastq")
                read_name_to_amplicon[read_name] = None

        if args.pickle:
            pickle.dump(read_name_to_amplicon, open(args.pickle, 'wb'), -1)

        logging.shutdown()


if __name__ == '__main__':
    main()
