from path import Path


def htcf(output_token):
    d = Path('/scratch/gtac/GTAC_RUN_DATA/')
    assert d.exists()
    return d.joinpath(output_token).mkdir_p()
