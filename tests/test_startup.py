from builtins import str
import argparse
import pytest
import gtac.startup
import gtac.startup.link
import gtac.startup.data
import re
import io
from path import Path


def sample_to_link(fp):
    d = fp.parent

    def wrapper(_, samp, iread):
        return d.joinpath('{0}.R{1}.fq.gz'.format(samp, iread))

    return wrapper


@pytest.fixture
def data_dir():
    return Path(__file__).abspath().dirname().joinpath('fake_seq_data')


@pytest.fixture
def factory(data_dir):
    d = data_dir
    factory = gtac.startup.SequenceDataFactory()
    factory.register(
        gtac.startup.HiseqData,
        demux_base=d,
        sample_key_base=d.joinpath('samplekeys'))
    factory.register(gtac.startup.data.HiseqDataIndex, demux_base=d)
    factory.register(gtac.startup.MiseqData, demux_base=d)
    factory.register(gtac.startup.data.RawMiseqDemux)
    factory.register(gtac.startup.DirectoryData)

    return factory

    # def test_miseq_sample_to_fastq(factory):
    #     seq_data = factory.build('AJJER')
    #     samp_to_fq = seq_data.get_sample_to_fastq()
    #     assert map(lambda x: x.basename(), samp_to_fq['9031-BP']) == ['9031-BP_S19_L001_R1_001.fastq.gz', '9031-BP_S19_L001_R2_001.fastq.gz']


def test_miseq_results_sample_to_fastq(factory):
    seq_data = factory.build('r305')
    samp_to_fq = seq_data.get_sample_to_fastq()
    assert [x.basename() for x in samp_to_fq['Lu7']] == [
        'Lu7_S2_L001_R1_001.fastq.gz', 'Lu7_S2_L001_R2_001.fastq.gz'
    ]


def test_miseq_demux_sample_to_fastq(factory, data_dir):
    p = data_dir.joinpath('R305')
    seq_data = factory.build(p)
    samp_to_fq = seq_data.get_sample_to_fastq()
    assert [x.basename() for x in samp_to_fq['Lu7']] == [
        'Lu7_S2_L001_R1_001.fastq.gz', 'Lu7_S2_L001_R2_001.fastq.gz'
    ]


def test_directory_sample_to_fastq(factory, data_dir):
    p = data_dir.joinpath('formatted')
    seq_data = factory.build(p)
    samp_to_fq = seq_data.get_sample_to_fastq()
    assert samp_to_fq['test1'] == [
        p.joinpath('test1_R1.fq'),
        p.joinpath('test1_R2.fastq')
    ]
    assert samp_to_fq['test2'] == [p.joinpath('test2_1.fastq.gz')]
    assert 'fake' not in samp_to_fq


def test_hiseq_sample_to_fastq(factory):
    seq_data = factory.build('1931_3')
    samp_to_fq = seq_data.get_sample_to_fastq()
    assert samp_to_fq['jc_stl17_4hpf_3'][
        0].basename() == 's_3_withindex_sequence.txt_ACAGATA.fq.gz'
    assert samp_to_fq['jc_wt_4hpf_3'][
        0].basename() == 's_3_withindex_sequence.txt_CCAGGCA.fastq'

    # I added a fastq with index AAAAAAA that is not in sample key
    # Assert that dict has only number of samples in samplekey
    assert len(samp_to_fq) == 20
    assert None not in samp_to_fq


def test_hiseq_multiple_samplekeys(factory, data_dir):
    # Check to make sure multiple samplekeys raise error
    # in a non-interactive shell

    # factory should fall back to HiseqDataIndex
    seq_data = factory.build('1000_1')
    assert isinstance(seq_data, gtac.startup.data.HiseqDataIndex)

    # Constructor raises SeqDataError with a OSError string inside
    with pytest.raises(gtac.startup.data.SeqDataError) as e:
        gtac.startup.data.HiseqData(
            '1000_1',
            sample_key_base=data_dir.joinpath('samplekeys'),
            demux_base=data_dir)

        assert 'OSError' in str(e)


def test_hiseq_sample_to_fastq_sampleKey_constructor(data_dir):
    with open(data_dir.joinpath('samplekeys', '1931_3')) as fh:
        sampleKey = gtac.startup.data.read_gtac_samplekey(fh)

    seq_data = gtac.startup.data.HiseqData(
        '1931_3', sampleKey=sampleKey, demux_base=data_dir)

    samp_to_fq = seq_data.get_sample_to_fastq()
    assert len(samp_to_fq) == 20


def test_hiseq_sample_to_fastq_bad_sampleKey(data_dir):
    with open(data_dir.joinpath('samplekeys', '1931_3')) as fh:
        sampleKey = gtac.startup.data.read_gtac_samplekey(fh)

    # Add in duplicate sample names
    sampleKey.data[:2] = [
        gtac.startup.data.IndexPair(index='ACAGATA', sample=e.sample)
        for e in sampleKey.data[:2]
    ]
    with pytest.raises(gtac.startup.data.SeqDataError) as e:
        gtac.startup.data.HiseqData(
            '1931_3', sampleKey=sampleKey, demux_base=data_dir)
        assert 'Duplicate index' in str(e)


def test_hiseq_index_sample_to_fastq(factory):
    seq_data = factory.build('1930_1')
    samp_to_fq = seq_data.get_sample_to_fastq()
    assert samp_to_fq['ACAGATA'][
        0].basename() == 's_3_withindex_sequence.txt_ACAGATA.fq.gz'
    assert samp_to_fq['CCAGGCA'][
        0].basename() == 's_3_withindex_sequence.txt_CCAGGCA.fastq'


@pytest.mark.parametrize('data', ['r100', '555_5', 'fake_seq_data/'])
def test_non_existing_run(factory, data):
    with pytest.raises(argparse.ArgumentTypeError) as e:
        factory.build(data)
        # Not looking for default error
        assert 'registered methods' not in str(e)


def test_write_sample_table(factory, tmpdir):
    fp = tmpdir.mkdir('sub').join('samples.txt')
    seq_data = factory.build('R305')
    gtac.startup.write_sample_table(
        [seq_data], Path(fp), make_link_name=sample_to_link(Path(fp)))
    lines = fp.readlines()
    assert len(lines) == 49  # number of demuxed fastq pairs plus header
    assert len(re.findall(r'\.R1\.fq\.gz', ''.join(lines))) == 48
    assert len(re.findall(r'\.R2\.fq\.gz', ''.join(lines))) == 48
    assert lines[0] == 'sample\tread1\tlink1\tread2\tlink2\n'
    assert not re.search(r'\sNA\s', fp.read())  # assert there are no NAs

    # Now see if SampleTableReader can parse the written table with no errors
    with open(str(fp), 'r') as fh:
        reader = gtac.startup.link.SampleTableReader(fh)
        for line in reader:
            pass


def test_get_fetch_items(factory, tmpdir):
    fp = Path(tmpdir.mkdir('sub').join('samples.txt'))
    seq_data = factory.build('r305')
    gtac.startup.write_sample_table([seq_data], fp, sample_to_link(fp))

    fetch_items = gtac.startup.link.get_fetch_items(fp)
    assert len(fetch_items) == 48 * 2
    assert all(each.target.exists() for each in fetch_items)

    # test that an existing link filepath does not throw an error
    fetch_items[0].link.touch()
    gtac.startup.link.get_fetch_items(fp)


def test_get_fetch_items_bad_link_directory(factory, tmpdir):
    # test that a non-existing directory throws an error
    fp = Path(tmpdir.mkdir('sub').join('samples.txt'))
    seq_data = factory.build('r305')
    gtac.startup.write_sample_table([seq_data], fp,
                                    sample_to_link(Path('/fake/dir/file.txt')))
    with pytest.raises(IOError):
        gtac.startup.link.get_fetch_items(fp)


def test_fetch_fastq(factory, tmpdir):
    # Tests the fetch_fastq boilerplate on simple analysis directory
    dir_ = Path(tmpdir.mkdir('sub'))
    fp = dir_.joinpath('samples.txt')
    seq_data = factory.build('r305')
    gtac.startup.write_sample_table([seq_data], fp, sample_to_link(fp))
    gtac.startup.fetch_fastq(fp)
    assert len(dir_.glob('*.fq.gz')) == 48 * 2


def test_fetch_files_cat_method(factory, tmpdir):
    dir_ = Path(tmpdir.mkdir('sub'))
    files = [dir_.joinpath(e) for e in '123']
    files[0].write_text('hello')
    files[1].write_text('world')
    files[2].write_text('different')

    link_fn = lambda x: dir_.joinpath('a' if int(x.name) < 3 else 'b')
    items = [
        gtac.startup.link.FetchItem(target=e, link=link_fn(e)) for e in files
    ]
    gtac.startup.link.fetch_files(items, 'cat')

    assert dir_.joinpath('b').islink()
    assert not dir_.joinpath('a').islink()
    assert dir_.joinpath('a').text() == 'helloworld'
    assert files[0].text() == 'hello'


def test_sampleTableReader_single_end():
    table = '''sample,read1,link1
h3k27ac_nrl_3,/demux/100_1/s_1_withindex_sequence.txt_CCCCCCG.fq.gz,/work/100_1_test/h3k27ac_nrl_3.100_1.R1.fq.gz
'''
    fh = io.StringIO(str(table.replace(',', '\t')))
    reader = gtac.startup.link.SampleTableReader(fh)
    fetch = next(reader.__iter__())
    assert fetch == [
        gtac.startup.link.FetchItem(
            target=Path(
                '/demux/100_1/s_1_withindex_sequence.txt_CCCCCCG.fq.gz'),
            link=Path('/work/100_1_test/h3k27ac_nrl_3.100_1.R1.fq.gz'),
            metadata={'sample': 'h3k27ac_nrl_3'})
    ]


def test_get_fetch_items_double_end():
    # Tests double end linking and final line with no newline
    table = '''sample,read1,link1,read2,link2
48G_even,/scratch/gtac/analysis/16s_seq/gv9_validation/data/even/48G-even_S46_L001_R1_001.fastq.gz,/scratch/gtac/analysis/16s_seq/gv9_validation/paper_all_regions/Reads/48G_even.R1.fq.gz,/scratch/gtac/analysis/16s_seq/gv9_validation/data/even/48G-even_S46_L001_R2_001.fastq.gz,/scratch/gtac/analysis/16s_seq/gv9_validation/paper_all_regions/Reads/48G_even.R2.fq.gz
BEI,/scratch/gtac/analysis/16s_seq/gv9_validation/data/even/BEI_S36_L001_R1_001.fastq.gz,/scratch/gtac/analysis/16s_seq/gv9_validation/paper_all_regions/Reads/BEI.R1.fq.gz,/scratch/gtac/analysis/16s_seq/gv9_validation/data/even/BEI_S36_L001_R2_001.fastq.gz,/scratch/gtac/analysis/16s_seq/gv9_validation/paper_all_regions/Reads/BEI.R2.fq.gz'''

    fh = io.StringIO(str(table.replace(',', '\t')))
    fetch = gtac.startup.link.get_fetch_items(fh, raise_errors=False)
    assert len(fetch) == 4


def test_analysis_dir(tmpdir):
    top = Path(str(tmpdir.join('analysis')))
    adir = gtac.startup.AnalysisDirectory(top)
    adir.create()

    assert top.isdir()
    assert all(top.joinpath(d).isdir() for d in adir.dirs)

    assert adir.reads(
        sample='test', read=1) == top.joinpath('reads', 'test.R1.fq.gz')
    assert adir.reads() == top.joinpath('reads')
    assert adir.summary() == top.joinpath('summary.txt')

    #must call additional args as kwargs
    with pytest.raises(TypeError):
        adir.reads('test', 1)
