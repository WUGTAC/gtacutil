from subprocess import check_call
import sys

'''
Remember to properly handle soft links
Setting of proper file permissions should be done in a separate command
'''

def tarball_cp(file_list, output, basename='data.tar'):
    tarball = output.joinpath(basename)
    cmd = ['tar', '-chf', tarball] + file_list
    check_call(cmd, stderr=sys.stderr, stdout=sys.stdout)


def rsync(file_list, output):
    cmd = ['rsync', '-ruhRL', '--progress'] + file_list + [output]
    check_call(cmd, stderr=sys.stderr, stdout=sys.stdout)
