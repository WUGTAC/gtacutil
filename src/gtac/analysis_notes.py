#!/usr/bin/env python
'''
This script is used for generating sample keys for customer prepped samples.
Such samples will not have index sequences entered into the LIMS so GARP auto-demuxing fails
Information about the index + sample pairing as well as which samples are on which lanes
will be entered into the analysis notes in LIMS.
This script allows you to copy this information with minimal formatting into a file and
produce sample keys for each lane present from these records

Input file format:
@@ starts a record
1st line:
  A template string to name the output sample key to. example: 2100_{}_customer_alignment
This template has string.format method called on it with each of the lane numbers found below.

2nd line:
   identifies which samples belong to which lanes.
Let \WP be [^a-zA-Z0-9_ ] then this line has format below. Note the colon after Lane \d and commas separating sample names
Lane\d+: sample1\WP+, sample2\WP+, ...

3rd line:
 defines the index sample pairings. In general you can just paste in the part of the analysis notes
that contains information about the indexes.

The program will convert TruSeq numbers into basepairs.
It will also write out dual-indexes if there are 2 matches for a sample. The i7 indexes
are assumed to come before the i5 index information. If this is not true then the indexes will
be improperly swapped in the samplekey.

If the first line does not start with @@ then it is used to define sequences for named indexes
For example: Myindex1 ACTGATC, Myindex2 GGTACTG, ...

Warning, the sample names in the 2nd line need to case-insensitive match with sample names
in the 3rd line to work. Check the spaces.
Samples not present in the second line but present in the third line are ignored.
'''
from builtins import zip
from builtins import object
import re
import csv
import sys
import argparse
import collections as cl
import logging
from path import Path


class Index(object):

    dual_joiner = '-'

    @classmethod
    def factory(cls, token):
        for c in cls.__subclasses__():
            try:
                obj = c(token)
            except ValueError:
                continue
            else:
                return obj
        raise ValueError('Token %s was not recognized by any known '
                         'subclass %s' % (token, cls.__subclasses__()))

    @classmethod
    def multimatch(cls):
        return '|'.join(c.multimatch() for c in cls.__subclasses__())

    def add_i5(self, other):
        self.seq = self.dual_joiner.join([self.seq, other.seq])

    @property
    def _info(self):
        return (self.name, self.seq)

    def __hash__(self):
        return hash(self._info)

    def __eq__(self, other):
        return self._info == other._info

    def __str__(self):
        return self.name


class RawIndex(Index):

    name = 'index'

    #regexp_string = r'(?:[ACGT]{5,}(?:%s[ACGT]{5,})?)' % dual_joiner
    regexp_string = r'(?:[ACGT]{5,})'

    def __init__(self, seq):
        m = re.match(self.regexp_string + '$', seq, flags=re.I)
        if m is None:
            raise ValueError()
        self.seq = seq.upper()

    @classmethod
    def multimatch(cls):
        return cls.regexp_string


class NamedIndex(object):
    '''
    This is not a subclass of Index because this is not a valid
    constructor. A valid constructor is creating using mulitple
    inheritance with Index in make_index_subclass()
    '''
    _seqs = {}

    def __init__(self, name):
        self.name = name.lower()
        self.seq = self._seqs.get(self.name)

        if self.seq is None:
            raise ValueError()

    @classmethod
    def multimatch(cls):
        return '|'.join('(?:%s)' % name for name in cls._seqs)


class TruSeqIndex(Index):
    _seqs = {
        'truseq1': 'ATCACG',
        'truseq2': 'CGATGT',
        'truseq3': 'TTAGGC',
        'truseq4': 'TGACCA',
        'truseq5': 'ACAGTG',
        'truseq6': 'GCCAAT',
        'truseq7': 'CAGATC',
        'truseq8': 'ACTTGA',
        'truseq9': 'GATCAG',
        'truseq10': 'TAGCTT',
        'truseq11': 'GGCTAC',
        'truseq12': 'CTTGTA',
        'truseq13': 'AGTCAA',
        'truseq14': 'AGTTCC',
    }

    def __init__(self, name):
        m = re.match(r'truseq ?(\d+)', name, flags=re.I)
        if m is None:
            raise ValueError()

        self.name = 'truseq' + m.group(1)
        self.seq = self._seqs.get(self.name)

        if self.seq is None:
            raise ValueError()

    @classmethod
    def multimatch(cls):
        return '|'.join('(?:%s)' % cls.regexp(e) for e in cls._seqs)

    @classmethod
    def regexp(cls, name):
        return 'truseq ?{0}(?!\d)'.format(name.replace('truseq', ''))


class Record(cl.namedtuple('Record', 'template, samples, pairing, index')):
    __slots__ = ()
    start = '@@'

    @classmethod
    def parse(cls, data):
        raw = data.split(cls.start)
        index = None
        # Custom named index line is optionally the first line in file
        if raw[0]:
            logging.info('Found custom index line: %s' % raw[0])
            index = raw[0].strip()  # Will be passed to all records

        for record in raw[1:]:
            # slice to guard against record ending in \n
            raw_template, samples, pairing = record.split('\n')[:3]
            template = raw_template.strip()
            yield cls(template, samples, pairing, index)


def split_and_zip_regexp(split_regexp, line):
    '''
    Returns a list of tuples (str, re.match)
    '''
    # match refers to tokens matching split_regexp
    match_iter = list(split_regexp.finditer(line))
    # split and filter out empty entries that can occur at the start and end
    non_match_iter = [e for e in split_regexp.split(line) if e]

    if len(match_iter) != len(non_match_iter):
        logging.critical('Mismatched number of tokens for pat %s ;; '
                         'line %s ;; matching: %s ;; non-matching: %s',
                         split_regexp.pattern, line,
                         [m.group(0) for m in match_iter], non_match_iter)

    return list(zip(non_match_iter, match_iter))


def get_reverse_complement(seq):
    match = {'a': 't', 'c': 'g', 'g': 'c', 't': 'a'}
    return ''.join(match.get(e, 'n') for e in seq.lower()[::-1]).upper()


def make_index_subclass(line):
    clean = re.compile(r'^\W+|\W+$')
    seqs = {
        clean.sub('', name).lower(): seq_match.group(0)
        for name, seq_match in split_and_zip_regexp(
            re.compile(r'[ACGT]{5,}'), line)
    }
    cls = type('DynamicIndex', (NamedIndex, Index), {'_seqs': seqs})
    return cls


def make_lane_to_sample(line):
    '''
    Assumes that the sample names are separated by , or ; or :
    '''
    data = {}
    lane_regexp = re.compile(r'(lane ?\d+)', flags=re.I)
    split = lane_regexp.split(line)
    lane_id = '1'
    for token in split:
        if not token:
            continue
        elif lane_regexp.match(token):
            lane_id = re.sub(r'\D+', '', token)
        else:
            samples = [e.strip() for e in re.findall('[^,;:]+', token.strip())]
            logging.info('samples for lane %s are: %s' % (lane_id,
                                                          ', '.join(samples)))
            if not all(samples):
                raise ValueError('Empty sample name found in line %s' % line)

            data[lane_id] = samples
    return data


def make_sample_to_index(sample_list, line, i5=None, no_dual=False):
    index_regexp = re.compile(Index.multimatch(), flags=re.I)
    sample_to_index_tokens = cl.defaultdict(list)
    data = {}

    # Here we match every index and sample token to a known list of samples
    for s, inmatch in split_and_zip_regexp(index_regexp, line):
        matches = []
        for samp in sample_list:
            if re.search(r'(?:^|\W)%s(?:\W|$)' % re.escape(samp), s, re.I):
                matches.append(samp)
        if len(matches) == 1:
            ind = inmatch.group(0)
            sample_to_index_tokens[matches[0]].append(ind)
            logging.debug('Matched sample: %s to index %s on raw token: %s' %
                          (matches[0], ind, s))
        elif len(matches) > 1:
            logging.critical('Multiple samples: %s matched same token: %s' %
                             (', '.join(matches), s))

    # Here we pair dual indexes that matched to the same sample
    # Assumes that i7 index will come before i5 in the line
    for samp, indexes in sample_to_index_tokens.items():
        data[samp] = Index.factory(indexes[0])
        if no_dual:
            continue
        if i5 is not None and len(indexes) == 1:
            # If we have a default i5 index set and we have only found a
            # single index for the sample we add default seq to be processed
            logging.debug('Adding default i5 adapter: %s to sample: %s', i5,
                          samp)
            indexes.append(i5)
        for each in indexes[1:]:
            data[samp].add_i5(Index.factory(each))

    unmatched = set(sample_list) - set(data.keys())
    if unmatched:
        logging.critical('These samples were not matched to an index: %s' %
                         ', '.join(unmatched))
    return data


def process_record(record, i5=None, rev_comp=False, no_dual=False):
    '''
    Returns a dict of lane name to list of lists. Each sub-list contains
    the info that will be written to a row in the sample key
    '''
    logging.info('Processing record for %s' % record.template)

    if record.index is not None:
        make_index_subclass(record.index)

    lane_to_sample = make_lane_to_sample(record.samples)
    sample_list = sum((lst for lst in list(lane_to_sample.values())), [])
    sample_to_index = make_sample_to_index(
        sample_list, record.pairing, i5=i5, no_dual=no_dual)

    lane_keys = {}
    for lane, lane_samples in lane_to_sample.items():
        output = []
        for samp in lane_samples:
            ind = sample_to_index.get(samp)
            if ind is None:
                logging.critical(
                    'There is a sample with no index match, aborting')
                raise ValueError()
            seq = get_reverse_complement(ind.seq) if rev_comp else ind.seq
            output.append([ind.name, seq, samp])
        lane_keys[lane] = output
    return lane_keys


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-di',
        '--dual_index',
        help='joins this sequence to all written indexes with a -')
    parser.add_argument(
        '--no_dual',
        action='store_true',
        help=
        'only write out i7 indexes and no i5 indexes even if some are found')
    parser.add_argument('input', type=Path)
    parser.add_argument(
        '-rc',
        '--rev_comp',
        action='store_true',
        help='write out the rev comp of all indexes found')
    parser.add_argument(
        '-o',
        '--output',
        type=Path,
        default='/scratch/ref/gtac/samplekeys',
        help='directory to place output sample keys in')
    return parser


def main():
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    parser = get_parser()
    args = parser.parse_args()
    with open(args.input, 'r') as fh:
        data = fh.read()

    for record in Record.parse(data):
        lane_keys = process_record(
            record,
            i5=args.dual_index,
            rev_comp=args.rev_comp,
            no_dual=args.no_dual)

        for lane, key_info in lane_keys.items():
            out_fp = args.output.joinpath(record.template.format(lane))
            logging.info('Writing file %s', out_fp)
            if out_fp.exists():
                logging.warn('Output file %s exists. Overwriting!', out_fp)
            with open(out_fp, 'w') as fh:
                writer = csv.writer(fh, delimiter='\t')
                for row in key_info:
                    writer.writerow(row)


if __name__ == '__main__':
    main()
