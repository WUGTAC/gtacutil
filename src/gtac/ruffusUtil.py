from builtins import str
from builtins import map
import os
import functools
import logging
import threading
import collections
from path import Path
from datetime import datetime

#Define a slightly higher logging level to filter out ruffus crap
# and only keep qsub argument messages
SUBMIT_JOB_LOGGING = 25

logging.addLevelName(SUBMIT_JOB_LOGGING, 'SUBMIT')


def auto_drm_job_factory(submitter, job_namer):
    '''
    job_namer takes job's output fp as sole argument
    DRM_logdir is directory where all jobs stdout and stderr
    streams are written.
    '''

    def decorator(func):
        @touch_wrapper
        @functools.wraps(func)
        def wrapper(in_file, out_file, *args, **kwargs):
            #hold must be a list of job names
            if hasattr(in_file, '__iter__'):
                hold = list(map(job_namer, in_file))
            else:
                hold = [job_namer(in_file)]

            #If the recipe has multiple output files just name the job
            # after the first one
            if hasattr(out_file, '__iter__'):
                name = job_namer(out_file[0])
            else:
                name = job_namer(out_file)

            submit_opts = {'name': name,
                           'hold': hold}

            cmd, resource = func(in_file, out_file, *args, **kwargs)

            submit_opts['resource'] = resource

            job_info = submitter.submit_job(cmd, **submit_opts)

            logging.log(
                SUBMIT_JOB_LOGGING,
                str(datetime.now()) + ' - '+\
                repr(out_file) + ' - '+\
                repr(os.path.basename(job_info.script))
                )

        return wrapper

    return decorator


def base_job_namer(prefix, fp):
    return '_'.join([prefix, os.path.basename(fp)])


def name_job_factory(job_namer, DRM_logdir='log/', logger_tuple=None):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(in_file, out_file, *args, **kwargs):
            if hasattr(in_file, '__iter__'):
                hold = list(map(job_namer, in_file))
            else:
                hold = [job_namer(in_file)]

            #If the recipe has multiple output files just name the job
            # after the first one
            if hasattr(out_file, '__iter__'):
                name = job_namer(out_file[0])
            else:
                name = job_namer(out_file)

            kwargs.update({'name': name, 'hold': hold, 'logdir': DRM_logdir})
            p = func(in_file, out_file, *args, **kwargs)

            logging.log(
                SUBMIT_JOB_LOGGING,
                str(datetime.now()) + ' - '+\
                repr(out_file) + ' - '+\
                repr(p.cmd) + ' ' + repr(p.call_args['in']),
                )

        return wrapper

    return decorator


def remove_empty_files(dir_):
    for fp in dir_.walkfiles():
        if fp.size == 0:
            fp.remove()


def submit_in_chain(func):
    '''
    Assumes that this is being used with name_job decorator
    Should be placed below name_job.
    '''
    _job_name_queue = collections.deque([None])
    _lock = threading.Lock()

    @functools.wraps(func)
    def wrapper(*args, **kwargs):

        new_hold = _job_name_queue.pop()

        if new_hold is not None:
            kwargs['hold'].append(new_hold)

        with _lock:
            _job_name_queue.append(kwargs['name'])

        return func(*args, **kwargs)

    return wrapper


def touch_wrapper(func):
    @functools.wraps(func)
    def wrapper(in_file, out_file, *args, **kwargs):
        ret = func(in_file, out_file, *args, **kwargs)

        #actually touch the file
        out_file_list = out_file if hasattr(out_file,
                                            '__iter__') else [out_file]
        for out_file in out_file_list:
            Path(out_file).touch()

        return ret

    return wrapper
