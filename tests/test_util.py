import os
import gzip
import pytest
from gtac import util


def test_atomic_write(tmpdir):
    final_fp = tmpdir.join('hello.txt')
    with util.atomic_write(str(final_fp)) as fh:
        fh.write('hello')
    assert final_fp.read() == 'hello'


def test_atomic_write_devnull(tmpdir):
    with util.atomic_write(os.devnull) as fh:
        fh.write('hello')


def test_atomic_write_subshell(tmpdir):
    '''
    Test to make sure that atomic_write waits for final cat command to finish
    '''
    final_fp = tmpdir.join('hello.txt')
    script = tmpdir.join('script.py')
    string = 'hello' * 100000
    cmd = '''import sys
import os
from gtac import util
print(sys.argv[1])
with util.atomic_write(sys.argv[1]) as fh:
 fh.write('{}')
print(os.path.getsize('{}'))'''.format(string, final_fp)
    script.write(cmd)

    bash = "python {0} >(gzip > {1})".format(script, final_fp)
    os.system('/bin/bash -c \'{}\''.format(bash))
    assert gzip.open(str(final_fp)).read().decode() == string

# yapf: disable
@pytest.mark.parametrize(
    'cmd,kwargs',
    [
        (['echo', 'hello'], {}),
        ('echo hello', {}),
        (['grep', '-o', 'hello'], {'input': 'hellogoobye'})
    ])
# yapf: enable
def test_run_command(cmd, kwargs):
    p = util.run_command(cmd, **kwargs)
    assert p.returncode == 0
    assert p.stdout == 'hello\n'
    assert p.stderr == ''


# yapf: disable
@pytest.mark.parametrize(
    'cmd,kwargs',
    [
        ('echo hello', {'input': 1}),
        ('echo hello', {'input': 0}),
    ])
# yapf: enable
def test_run_bad_input(cmd, kwargs):
    with pytest.raises(TypeError):
        util.run_command(cmd, **kwargs)

# def test_atomic_write_subprocess(tmpdir):
#     final_fp = tmpdir.join('hello.txt')
#     string = 'hello' * 100000
#     p = sp.Popen('gzip -c - > {}'.format(final_fp), shell=True)
#     fp = '/dev/fd/{}'.format(p.stdin.fileno())
#     with util.atomic_write(fp) as fh:
#         fh.write(string)
#     assert gzip.open(final_fp).read().decode() == string
