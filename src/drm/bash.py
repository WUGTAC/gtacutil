import os

import sh
from jinja2 import Environment, DictLoader

import drm.base as base


def make_environment(template_dict):
    def format_stderr(logDir, script_name):
        return os.path.join(logDir, script_name) + '.stderr'

    def format_stdout(logDir, script_name):
        return os.path.join(logDir, script_name) + '.stdout'

    env = Environment(loader=DictLoader(template_dict))
    env.filters['format_stderr'] = format_stderr
    env.filters['format_stdout'] = format_stdout
    return env


template_dict = {
    'job':
    '''
#!/bin/bash
set -x
cd {{ workDir }}
{

{{ job }} 

} 1> {{ logDir| format_stdout(script_name) }} \
2> {{ logDir| format_stderr(script_name) }}
'''
}

_ENV = make_environment(template_dict)


class Resource(base.Resource):
    def __str__(self):
        return ''


class MpiResource(base.MpiResource):
    def __str__(self):
        return ''


class Constraint(base.Constraint):
    def __str__(self):
        return ''


class Submitter(base.Submitter):
    template = _ENV.get_template('job')

    def submit(self, script_fp):
        sh.bash(script_fp)
        return None


class Waiter(base.Waiter):
    def wait(self):
        return
